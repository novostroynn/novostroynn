<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CKEditor 5 – Classic editor</title>
    <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
</head>
<body>
    <h1>Classic editor</h1>
    <div class="text_editor">
	<textarea name="content" id="editor">
        &lt;p&gt;This is some sample content.&lt;/p&gt;
    </textarea>
    </div>
    
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
</body>
</html>


<?php 

$house_progress = [
    [0] => [
            'id' => 1,
            'description' => '',
            'meta_description' => '', 
            'src' => 'images/house/progress/2_house_progress_3e1b126d545a3bdcf97adf202f4e9944.jpg',
            'house_id' => 2,
            'year' => 2015,
            'month' => 1,
            'order' => 0,
            'name' => Январь,
            'img_id' => 11
        ],

    [1] => [
        [
            'id' => 1,
            'description' => '',
            'meta_description' => '',
            'src' => 'images/house/progress/2_house_progress_acebe2c709f7f4f07b4d2ef81cb9cda2.jpg',
            'house_id' => 2,
            'year' => 2015,
            'month' => 1,
            'order' => 0,
            'name' => Январь,
            'img_id' => 12
        ],

    [2] => 
        [
            'id' => 15,
            'description' => '',
            'meta_description' => '',
            'src' => 'images/house/progress/2_house_progress_234860f5b6ea42e6249282e8aea1d3e2.jpg',
            'house_id' => 2,
            'year' => 2016,
            'month' => 15,
            'order' => 0,
            'name' => Март,
            'img_id' => 14
        ],

    [3] => 
        [
            'id' => 16,
            'description' => '',
            'meta_description' => '',
            'src' => 'images/house/progress/2_house_progress_a48620b3abd142f8223f608f63ce28f7.jpg',
            'house_id' => 2,
            'year' => 2016,
            'month' => 16,
            'order' => 0,
            'name' => Апрель,
            'img_id' => 13
        ]

]
    
    <?php 
for($i=0; $i < count($house_progress); $i++){
    if( !isset( $house_progress[$i-1]['year'] ) ){ : ?>
	<!--- первый месяц првого года -->
		    <h2> {{ $house_progress[$i]->year }} </h2>
		    <br><b> {{ $house_progress[$i]->name }}</b><br>
			<!-- Открываем блок info_wrap после названия первого месяца  первого года  -->
		    <div class="info_wrap sortable"> 
			<div class="info">
			    <img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
				data-id="{{$house_progress[$i]->img_id}}"
				src="/storage/{{ $house_progress[$i]->src }}" 
				alt="{{$house_progress[$i]->meta_description}}">
			    <label for="description">Описание</label>
			    <input type="text" name="description" class="description" 
				   value="{{$house_progress[$i]->description}}">
			    <label for="meta_description">Мета описание</label>
			    <input type="text" name="meta_desription" class="meta_description"
				   value="{{$house_progress[$i]->meta_description}}">
			    <div class="info_wrap">
				<button class="delete_progress">Удалить</button>
				<button class="change_description">Обновить</button>
			    </div>
			    <button class="update_order">Обновить порядок картинок</button>
			</div>
<?php elseif( $house_progress[$i]['year'] == $house_progress[$i-1]->year ){ 
	    if( $house_progress[$i]->month != $house_progress[$i-1]->month ){ ?>
	    <!-- выводим блок нового месяца -->
		    </div> <!-- Закрываем блок info_wrap когда в этом месяце больше нет картинок -->
	    
		    <br><b> {{ $house_progress[$i]->name }} </b><br>
			<!-- открываем блок info_wrap после названия следующего месяца этого года-->
		    <div class="info_wrap sortable">
		    <div class="info">
			<img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
			     data-id="{{$house_progress[$i]->img_id}}"
			     src="/storage/{{ $house_progress[$i]->src }}" 
			     alt="{{$house_progress[$i]->meta_description}}">
			<label for="description">Описание</label>
			<input type="text" name="description" class="description"
			       value="{{$house_progress[$i]->description}}">
			<label for="meta_description">Мета описание</label>
			<input type="text" name="meta_desription" class="meta_description"
			       value="{{$house_progress[$i]->meta_description}}">
			<div class="info_wrap">
			    <button class="delete_progress">Удалить</button>
			    <button class="change_description">Обновить</button>    
			</div>
			<button class="update_order">Обновить порядок картинок</button>
		    </div>
	    <?php   }else{ ?>
	<!-- следующая картинка месца -->
			<div class="info">
			    <img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
				 data-id="{{$house_progress[$i]->img_id}}"
				 src="/storage/{{ $house_progress[$i]->src }}" 
				 alt="{{$house_progress[$i]->meta_description}}">
			    <label for="description">Описание</label>
			    <input type="text" name="description" class="description"
				   value="{{$house_progress[$i]->description}}">
			    <label for="meta_description">Мета описание</label>
			    <input type="text" name="meta_desription" class="meta_description"
				   value="{{$house_progress[$i]->meta_description}}">
			    <div class="info_wrap">
				<button class="delete_progress">Удалить</button>
				<button class="change_description">Обновить</button>
			    </div>
			</div>
	    <?php   }
		    
}else{ :?>
		    <!-- закрываем блок info_wrap когда больше нет картинок в последнем месяце года -->
	</div> 
	<hr>
	<h2> {{ $house_progress[$i]->year }}</h2>
	<br><b> {{ $house_progress[$i]->name }}</b><br>
	<div class="info_wrap sortable"> <!-- открываем блок info_wrap в первом месяце следующего года -->
	    <div class="info">
		<img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
		     data-id="{{$house_progress[$i]->img_id}}"
		     src="/storage/{{ $house_progress[$i]->src }}" 
		     alt="{{$house_progress[$i]->meta_description}}">
		<label for="description">Описание</label>
		<input type="text" name="description" class="description"
		       value="{{$house_progress[$i]->description}}">
		<label for="meta_description">Мета описание</label>
		<input type="text" name="meta_desription" class="meta_description"
		       value="{{$house_progress[$i]->meta_description}}">
		<div class="info_wrap">
		    <button class="delete_progress">Удалить</button>
		    <button class="change_description">Обновить</button>
		</div>
		<button class="update_order">Обновить порядок картинок</button>
	    </div>
    }
		    
}
		

