    // скрип яндекс карты
ymaps.ready(init);
var myMap;



function init(){   
        // создаем объект карты
    myMap = new ymaps.Map ("map", {
            // центр карты по координатам объекта
        center: [56.316203, 44.003926],
        controls: ['zoomControl'],
        zoom: 12
    });
        // вывод точки на карту
    var myPlacemark = new ymaps.Placemark(
            // кординаты 
        [56.32, 44.01],
        {},
                // курсор на карте
        {
            draggable: true, // метку можно перемещать
            preset: 'twirl#blueStretchyIcon' // используемое изображение
        }
    );
        // при событии перемещения курсора 
    myPlacemark.events.add('dragend', function(e) {
            // Получение ссылки на объект, который был передвинут.
        var thisPlacemark = e.get('target');
            // Определение координат метки, получаем массив
        var coords = thisPlacemark.geometry.getCoordinates();
            // и вывод их в необходимые поля на странице
        latitude = document.getElementById('latitude');
        longitude = document.getElementById('longitude');
        latitude.value = coords[0];
        longitude.value = coords[1];
    });
        // добавляем точку на карту
    myMap.geoObjects.add(myPlacemark);
}




//    myGeoObject = new ymaps.GeoObject({
//        geometry: {
//            type: "Point",// тип геометрии - точка
//            coordinates: [56.31, 44.00] // координаты точки
//       }
//    });

