 // скрип яндекс карты
ymaps.ready(init);
var myMap;



function init(){   
        // создаем объект карты
    myMap = new ymaps.Map ("map", {
            // центр карты по координатам объекта
        center: [56.316203, 44.003926],
        controls: ['zoomControl'],
        zoom: 12
    });
        // вывод точек на карту


    var myGeoObjects = [];

    for (var i = 0; i<coords.length; i++) {
        var baloon_string = '<div class="baloon_wrap">'+
                        '<div class="baloon_img">'+
                            '<img src="/storage/'+coords[i].logo_src+'" /> '+
                        '</div>'+
                        '<div class="estate_desc">';
                            if( coords[i].name !== null){
                                baloon_string += '<h3>'+coords[i].name+'</h3>';
                            }
                            if( coords[i].address !== null){
                                baloon_string += '<p>Адрес: '+coords[i].address+'</p>';
                            }
                            if( coords[i].location_area !== null){
                                baloon_string += '<p> Район: '+coords[i].location_name+'</p>';
                            }
                            if( coords[i].end_construct !== null){
                                baloon_string += '<p>Срок сдачи: '+coords[i].end_construct+'</p>';
                            }
  
        baloon_string +=    '</div>'+
                        '</div>'; 
        myGeoObjects[i] = new ymaps.GeoObject({
            geometry: {
                type: "Point",
                coordinates: [coords[i].latitude, coords[i].longitude]
                //coordinates: [56.316203, 44.003926]
            },
            properties: {
                clusterCaption: 'Геообъект № '+(i+1),
                balloonContentBody: baloon_string
            }
        });
    }

    var myClusterer = new ymaps.Clusterer();
    myClusterer.add(myGeoObjects);
    myMap.geoObjects.add(myClusterer);
    
}
