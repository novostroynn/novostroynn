<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1" /> 
        <title>Панель администратора</title>
        <script type="text/javascript" src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
	
        <link rel="stylesheet" href="{{ URL::asset('css/manager.css') }}">
	
	<link rel="stylesheet" href="{{ URL::asset('js/jquery-1.12.1.custom/jquery-ul.css') }}">
    </head>
    <body>
        <div id="message">qwe</div>
        <div class="wrap">
            <header>
                <div class="header-menu">
                    <div class="links">
                        <a href="/manager">
                            На главную
                        </a>  
                    </div>
                    <div class="user_button">
                        <a href="/logout">
                            Выйти
                        </a>  
                    </div>
                </div>  
            </header>
            <div class="page-wrap">
                @yield('content')
            </div>
            <footer>
                Новостройки Нижнего Новгорода 2019
            </footer>     
        </div>
        
    </body>
</html>
