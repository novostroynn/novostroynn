<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Админка</title>
        
        <link rel="stylesheet" href="{{URL::asset('css/manager.css')}}">
    </head>
    <body>
        <div class="wrap">
            <header></header>
            <div class="page-wrap">
                @yield('authorized_form')
            </div>
            <footer>
                
            </footer>
        </div>
    </body>
</html>

