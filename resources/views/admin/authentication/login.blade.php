@extends('admin.authentication.index')
@section('authorized_form')
<div class='authorization-form-wrap'>
    <form action="{{ url('/login') }}"class="authorization-form" method='POST'>
        {{ csrf_field() }}
        <label for="login">Ваш логин:</label>
        <input type="text" name='login' placeholder="Логин">
        <label for="password">Пароль:</label>
        <input type="password" name='password' placeholder="пароль">
        <input type="submit" value='Войти'>
    </form>
</div>
@endsection

