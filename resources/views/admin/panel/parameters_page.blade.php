@extends('admin.layouts.panel')
@section('content')
<div class="button_panel">
    <p class="panel_category">Разделы</p>
</div>
<div class="workspace">
    <p>Список параметров объектов</p>
    <ul class="parameters_list_wrap">
	{{csrf_field()}}
        <li>
            @include('admin.panel.parameters.area')
        </li>
        <li>
            @include('admin.panel.parameters.parking')
        </li>
        <li>
            @include('admin.panel.parameters.main_structure')
        </li>
        <li>
            @include('admin.panel.parameters.wall_material')
        </li>
        <li>
            @include('admin.panel.parameters.window_installation')
        </li>
        <li>
            @include('admin.panel.parameters.cooker_type')
        </li>
        <li>
            @include('admin.panel.parameters.elevator_type')
        </li>
        <li>
            @include('admin.panel.parameters.apartment_finishing')
        </li>
        <li>
            @include('admin.panel.parameters.heating_type')
        </li>
        <li>
            @include('admin.panel.parameters.construct_date')
        </li>
	<li>
	    @include('admin.panel.parameters.apartment_categories')
	</li>
	<li>
	    @include('admin.panel.parameters.apartment_types')
	</li>
    </ul>
</div>
<script type="text/javascript">
    $('document').ready(function(){
	    // показать/скрыть блок с параметрами 
        $('.show_block').click(function(){
            $(this).hide();
            $(this).siblings('.hide_block').show();
            $(this).parent().siblings('.parameter_list').css({
                'display': 'block'
            });
        });
        $('.hide_block').click( function(){
            $(this).siblings('.show_block').show();
            $(this).hide();
            $(this).parent().siblings('.parameter_list').css({
                'display': 'none'
            });
        });
	    
		// по двойному клику на параметр, берем его текст и перемещаем в input
		// вместо кнопки сохранить показываем кнопку обновить
        $('.parameter').dblclick(function(){
	    var parameter = $(this);
	    
	    var input = parameter.parent('.parameter_wrap').siblings('.add_parameter')
		    .children('form').children('input[type=text]');
	    input.val(parameter.text());
	    
	    input.siblings('input[type=button]').hide();
	    input.siblings('.update_parameter').show();
	    
	    var parameter_id = $(this).data('id');
	    
	    
	    input.siblings('.update_parameter').data({'parameter_id': parameter_id});
	    qwe = input.siblings('.update_parameter').data('parameter_id');
	    console.log(qwe);
	    
	    return false;
	});
	
	$('body').on('click','.update_parameter', function(){
	    var table_name = $(this).data('table');
	    var token = '<?php echo csrf_token() ?>';
	    var input = $(this).siblings('input[type=text]');
	    var name = input.val();
	    var parameter_id = $(this).data('parameter_id');
	    
	    $.post(
		'/parameter/'+parameter_id+'/'+table_name,
		{'name':name, '_token':token  },
		function(data){
		    input.val('');
		    input.siblings('input[type=button]').show();
		    input.siblings('.update_parameter').hide();
		    if(data === 0){
			$('#message').show().html('Ошибка обновления параметра');
			$('#message').delay(2000).hide(500).html('');
		    }else{
			$('#message').show().html('Обновлено успешно.');
			$('#message').delay(2000).hide(500).html();
		    }
		}
	    );
	    $(this).parent('form').parent('.add_parameter').siblings('.parameter_wrap').children('span[data-id="'+parameter_id+'"]').text(name);
	    
	    return false; 
	});
        
    });
</script>
@endsection

