@extends('admin.layouts.panel')
@section('content')
<div class="button-panel">
<a href="/manager/builder/create">Создать</a>
<style>
    img{
	width: 150px;
	
    }
</style>   
</div>
<div class="workspace">
    <h1>Список застройщиков</h1>
    <table>
	<tr>
	    <th>№</th>
	    <th>Логотип</th>
	    <th>Название</th>
	    <th>Адрес</th>
	    <th>Телефон</th>
	    <th>Email</th>
	    <th>сайт</th>
	    <th>опубликован</th>
	</tr>
	@foreach($builders as $k => $builder)
	<tr>
	    <td></td>
	    <td><img style="width: 100px;" src="{{ URL::asset('storage/'.$builder->logo_src) }}" alt='логотип застройщика'</td>
	    <td><a href="/manager/builder/{{$builder->id}}/edit" >{{$builder->name}}</a></td>
	    <td>{{$builder->email}}</td>
	    <td>{{$builder->address}}</td>
	    <td>{{$builder->phone}}</td>
	    <td>{{$builder->logo_src}}</td>
	    <td>{{$builder->published}}</td>
	</tr>
	@endforeach
	<tr>
	    <td></td>
	</tr>
    </table>
<!--    @if(isset($path))
	первый файл {{$path}}
	<img src="{{URL::asset('storage/'.$path) }}" alt="">
	второй файл
	<img src="{{URL::asset('storage/images/logo2/2.jpg') }}" alt="">
@else
второй файл
<img src="{{URL::asset('storage/not_public/images/logo2/админка.jpg ') }}" alt="">
<img src="{{URL::asset('storage/images/logo/1.jpg') }}" alt="">
<img src="{{URL::asset('images/logo2/2.jpg') }}" alt="">

    @endif-->
<!--    //<?php  //echo asset('storage/1.jpg'); ?> -->
<!--    <img src="{{URL::asset('storage/1.jpg') }}" alt="">-->
    
</div>
@endsection