@extends('admin.layouts.panel')
@section('content')
<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::asset('/js/ckeditor/ckeditor.js')}}"></script>
<div class="button-panel">
<a href="/manager/builder">К списку </a>
    
</div>
<div class="workspace">
    <h1>добавить застройщика</h1>
    <form action="/manager/builder" enctype="multipart/form-data" method="POST">
	<input type="hidden" name="type_agent" value="1">
	{{csrf_field()}}
    <div class="info_wrap">
	    
	<div class="info">
<!--		<label for="logo">логотип</label>
логотип создается позже
		<input type="file" name="logo_src" accept="">-->
		<label for=""><p>Карта</p></label>
		<p>Широта: <input type="text" name="latitude" id="latitude" value="56.32">
		Долгота: <input type="text" name="longitude"  id="longitude" value="44.01"></p>
		<div class="complex_map" id="map" style="width: 500px; height: 400px">     
		</div> 
		
	    </div>

	    <div class="info">
		<div class="info_wrap">
		    <label for="published">Опубликовано</label>
			<input type="radio" name="published" value="0" checked="checked">Нет
			<input type="radio" name="published" value="1">Да 
		</div>
		<hr>
		<label for="name">название</label>
		<input type="text" name="name">
		<label for="address">Адрес</label>
		<input type="text" name="address">
		<label for="phone">телефон</label>
		<input type="text" name="phone">
		<label for="email">Email</label>
		<input type="text" name="email">
		<label for="site">Сайт</label>
		<input type="text" name="site">
	    </div>
	    
	
	
	
    </div> <!-- info_wrap -->
    <div class="info">
	<label for="description">описание</label>
	<textarea name="description" id="editor1" rows="10" cols="200"></textarea>
    </div>
	<input type="submit" id="save_builder" class="workspace_save" value="Сохранить застройщика">
    </form>
</div> <!--	workspace -->
<script>
    /* инициализация визуального редактора */
 CKEDITOR.replace( 'editor1' );
 </script>
<script src= "{{ URL::asset('js/ymaps.js') }}">
@endsection