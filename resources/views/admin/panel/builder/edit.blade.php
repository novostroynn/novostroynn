@extends('admin.layouts.panel')
@section('content')
<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::asset('/js/ckeditor/ckeditor.js')}}"></script>
<div class="button-panel">
<a href="/manager/builder">К списку </a>
    
</div>
<div class="workspace">
    <h1>{{ $builder->name }}</h1>
    <form action="/manager/builder/{{$builder->id}}" enctype="multipart/form-data" method="POST">
	<input type="hidden" name="type_agent" value="1">
	<input type="hidden" name="_method" value="PUT">
	{{csrf_field()}}
    <div class="info_wrap">
	    
	<div class="info">
	    <label for="logo">Логотип:</label>
	    @if(!$builder->logo_src)
		<input type="file" name="logo_src" accept="">
		<label for="logo_description">Название логотипа</label>
		<input type="text" name="logo_description" value="{{$builder->logo_description}}">
	    @else
	    <img class="logo_img" src="{{ URL::asset('/storage/'.$builder->logo_src) }}" alt="{{$builder->logo_description}}">
	    <input type="hidden" name="logo_src" value="{{ $builder->logo_src }}" >
	    <label for="logo_description">Название логотипа</label>
	    <input type="text" name="logo_description" value="{{$builder->logo_description}}">
	    @endif
		
	    <label for=""><p>Карта</p></label>
	    <p>Широта: <input type="text" name="latitude" id="latitude" value="{{$builder->latitude}}">
	    Долгота: <input type="text" name="longitude"  id="longitude" value="{{$builder->longitude}}"></p>
	    <div class="complex_map" id="map" style="width: 500px; height: 400px">     
	    </div> 
	</div>
	
	<div class="info">
	    <div class="info_wrap">
		<label for="published">Опубликовано</label>
		@if( $builder->published == 0 )
		    <input type="radio" name="published" value="0" checked="checked">Нет
		    <input type="radio" name="published" value="1">Да
		@else
		    <input type="radio" name="published" value="0"  >Нет
		    <input type="radio" name="published" value="1" checked="checked" >Да
		@endif

	    </div>
	    <label for="name">название</label>
	    <input type="text" name="name" value="{{$builder->name}}">
	    <label for="address">Адрес</label>
	    <input type="text" name="address" value="{{$builder->address}}">
	    <label for="phone">телефон</label>
	    <input type="text" name="phone" value="{{$builder->phone}}">
	    <label for="email">Email</label>
	    <input type="text" name="email" value="{{$builder->email}}">
	    <label for="site">Сайт</label>
	    <input type="text" name="site" value="{{$builder->site}}">
	</div>
	    
	
	
	
    </div> <!-- info_wrap -->
    <div class="info">
	<label for="description">описание</label>
	<textarea name="description" id="editor1" rows="10" cols="200">{!! $builder->description !!}</textarea>
    </div>
	<input type="submit" id="save_builder" class="workspace_save" value="Сохранить изменения">
    </form>
</div> <!--	workspace -->
<script>
	/* инициализация визуального редактора */
    CKEDITOR.replace( 'editor1' );
</script>
<script type="text/javascript">
    (function($, undefined){
	$(function(){
	    $('.logo_img').dblclick(function(){
		$(this).parent('.info').prepend(
		    '<input type="file" name="logo_src" accept="">'
		);
	    });
	});
})(jQuery);
    </script>

<script>
        var coordinates = new Array();
        coordinates = [<?php echo  $builder->latitude; ?>, <?php echo  $builder->longitude; ?>];
       
       
    </script>
    
    
<script src= "{{ URL::asset('js/ymaps_edit.js') }}">
@endsection