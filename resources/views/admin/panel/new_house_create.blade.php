@extends('admin.layouts.panel')
@section('content')
<div class="button-panel">
    <a href="/manager/house_estates">Назад</a>
    <hr>
    <a href="">Предпросмотр</a>
    <a href="">Сохранить</a>
    <a href="">Отменить</a>
    <a href="">Закрыть</a>
</div>
<div class="workspace">
    <div class="info_wrap">
        <div class="info">
            <label for="house_name">Название дома</label>
            <input type="text" name="house_name">
            <label for="house_floors">Количество этажей</label>
            <input type="text" name="house_floors">
            <label for="house_entranses">Количество подъездов</label>
            <input type="text" name="house_entranses">
            <label for="house_apartments">Количество квартир</label>
            <input type="text" name="house_apartmens">
            <label for="house_start_construct">Начало строительства</label>
            <input type="date" name="house_start_construct">
            <div class="info_wrap">
                <div class="info">
                    <label for="house_end_construct">Срок сдачи</label>
                    <select name="house_end_construct" id="">
                        <option value="0">Срок сдачи</option>
                        <option value=""></option>
                    </select>
                </div>
                <div class="info">
                    <label for="house_is_built">Сдан</label>
                    <input type="checkbox" name="house_is_built">
                </div>
            </div>
            <label for="house_elevator">Наличие лифта</label>
            <select name="house_elevator" id="">
                <option value="1">нет</option>
                <option value="0">есть</option>
            </select>
            <label for="house_parking_type">Тип парковки</label>
            <select name="house_parking_type" id="">
                <option value="0">Тип парковки</option>
                <option value=""></option>
                <input type="text" name="house_parking_count">
            </select>
            
        </div> <!-- info -->
        <div class="info">
            <label for="house_basic_stricture">Несущая конструкция</label>
            <select name="house_basic_stricture" id="">
                <option value="0">Несущая конструкция</option>
                <option value=""></option>
            </select>
            <label for="house_wall_material">Материал стен</label>
            <select name="house_wall_meterial" id="">
                <option value="0">Материал стен</option>
                <option value="1">Кирпич</option>
            </select>
            <label for="house_apartment_finishing">Тип отделки квартир</label>
            <select name="house_apartment_finishing" id="">
                <option value="0">Тип отделки квартир</option>
                <option value=""></option>
            </select>
            <label for="house_ceiling_height">Высота потолков</label>
            <select name="house_ceiling_height" id="">
                <option value="0">Высота потолков</option>
                <option value=""></option>
            </select>
            <label for="house_heating">Тип отопления</label>
            <select name="house_heating_type" id="">
                <option value="0">Тип отопления</option>
                <option value=""></option>
            </select>
            <label for="house_cooker_type">Тип кухонной плиты</label>
            <select name="house_cooker_type" id="">
                <option value="0">Тип кухонной плиты</option>
                <option value=""></option>
            </select>
            <label for="house_glass_material">Материал остекления</label>
            <select name="house_glass_material" id="">
                <option value="0">Материал остекления</option>
                <option value=""></option>
            </select>
        </div> <!-- info -->
    </div> <!-- info_wrap -->
    <div class="house_layuot_galery">
        <label for="house_layout"><p>Планировка дома</p></label>
        <input type="button" value="Добавить">
        <input type="button" value="Удалить">
        <div class="gallery_img">
            <div class="img"></div>
            <div class="img"></div>
            <div class="img"></div>
            <div class="img"></div>
            <div class="img"></div>
        </div>
        <label for="house_layuot_galery">Ход строительства дома</label>
        <div class="house_construction_progress">
            <p>Относится только к этому дому</p>
            <p>Делятся на года и месяцы</p>
            <p>несколько штук чего-то</p>
            <p>добавить удалить что-то</p>
            пока непонятно что тут есть и как это все хранить и редактировать
        </div>
    </div> <!-- house_layuot_galery -->
    <div class="appartments_table">
        <p>Таблица квартир</p>
        <table>
            <tr>
                <th>Комнат</th>
                <th>Этаж</th>
                <th>S площадь</th>
                <th>S жилая</th>
                <th>S кухни</th>
                <th>Тип отделки</th>
                <th>Цена</th>
                <th>Планировка</th>
                <th>Какие-то кнопки</th>
            </tr>
        </table>
    </div>
</div> <!-- workspace -->
@endsection
