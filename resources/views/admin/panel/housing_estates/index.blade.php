@extends('admin.layouts.panel')
@section('content')
<div class="button-panel">
    <a href="/manager/housing_estates/create">Создать</a>
    
</div>
<div class="workspace">
    <p>Список объектов новостроек</p>
    <table id="housing_estates_table">
        <tr>
            <th>Номер</th>
	    <th>Логотип</th>
            <th>Название</th>
            <th>Дата создания</th>
            <th>Автор</th>
            <th>Дата изменения</th>
            <th>Автор изменений</th>
            <th>Опубликовано</th>
            <th>Расположение на сайте</th>
        </tr>
        @foreach($housing_estates as $num => $housing_estate)
        <tr>
            <td>{{ $num+1 }}</td>
            <td><img src="/storage/{{$housing_estate['logo_src']}}" alt="{{$housing_estate['logo_description']}}"></td>
                <td><a href="/manager/housing_estates/{{$housing_estate['id']}}/edit">{{ $housing_estate['name']}}</a></td>
            
            <td>{{ $housing_estate['created_at'] }}</td>
            <td>автор</td>
            <td>{{ $housing_estate['updated_at'] }}</td>
            <td>автор изменений</td>
            <td>{{ $housing_estate['published'] }}</td>
            <td>[......]</td>
        </tr>
        @endforeach
    </table>
    
</div>
@endsection

