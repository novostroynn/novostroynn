@extends('admin.layouts.panel')
@section('content')
<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::asset('/js/ckeditor/ckeditor.js')}}"></script>
<div class="button-panel">
    <a href="/manager/housing_estates">Назад</a>
    <hr>
</div>
<div class="workspace">
    <div class="housing_complex">
        <div class="tabs">
            <label for="for_tab1" class="tab1 tab_button">Информация о ЖК </label>
        </div>
        <div class="for_tab1">
            <form action="/manager/housing_estates" method="POST">
            <div class="info_wrap">
                
                    <div class="info">
                        <label for="name"><p>Название ЖК</p></label>
                        <input type="text" name="name" />
<!--                        <label for="alias"><p>Псевдоним</p></label>
                        <input type="text" name="alias">-->
			<input type="hidden" name="creater" value="{{$user_id}}">
                        
                        <div class="info_wrap">
                            <label for="builder"><p>Застройщик:</p> </label>
                            <select name="agents[]"  class = "agent" id="">
                                <option value="">Выберите застройщика</option>
				@foreach($builders as $builder)
				<option value="{{$builder->id}}">{{$builder->name}}</option>
				@endforeach
                            </select>
			</div>
			<div class="info_wrap">
			    <label for="seller">Продавец: </label>
			    <select name="agents[]" class = "agent" id="">
				<option value="">Выберите продавца</option>
				@foreach($sellers as $seller)
				    <option value="{{$seller->id}}">{{$seller->name}}</option>
				    @endforeach
			    </select>
			</div>
                        <div class="estate_location">
                            <label for="estate_location">Расположение:</label>
                           
                                {{ csrf_field() }} 
                            <select name="region" id='location_region'>
                                <option value="">Выберите регион</option>
                                @foreach($location_regions as $location_region)
                                    <option value="{{ $location_region['id'] }} ">{{ $location_region['name'] }}</option>
                                @endforeach
                            </select>
                                <select name="location_area" id="location_area">
                                    
                                </select>
                        </div>
                        <label for="address"><p>Адрес</p></label>
                        <input type="text" name="address">
                        <hr>
                        <label for=""><p>Карта</p></label>
                        <p>Широта: <input type="text" name="latitude" id="latitude" value="56.32">
                        Долгота: <input type="text" name="longitude"  id="longitude" value="44.01"></p>
                        <div class="complex_map" id="map" style="width: 500px; height: 400px">     
                        </div> 
                    </div> <!-- info -->
                    <div class="info">
                        <div class="published_radio">
                            <label for="published"><p>Опубликовано</p></label>
                            <input type="radio" name="published" value="1" >Да
                            <input type="radio" name="published" value="0" checked>Нет
                        </div>
                        <label for="start_construct">Начало строительства:</label>
                        <select name="start_construct" id="start_construct_date">
                            <option value="">Выберите дату</option>
                            @foreach($construct_dates as $construct_data)
                                <option value="{{ $construct_data['id'] }}">{{ $construct_data['name'] }}</option>
                            @endforeach
                        </select>
                        <div class="info_wrap">
                            <div class="info">
                                <label for="end_construct">Срок сдачи</label>
                                <select name="end_construct" id="end_construct_data">
                                    <option value="">Срок сдачи</option>
                                    @foreach($construct_dates as $construct_data)
                                        <option value="{{ $construct_data['id'] }}">{{ $construct_data['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <label for="count_houses"><p>Количество домов</p></label>
                        <input type="text" name="count_houses">
                        <label for="count_apartments"><p>Количество квартир в ЖК</p></label>
                        <input type="text" name="count_apartments">
                        <label for="main_structure"><p>Несущая конструкция</p></label>
                        <select name="main_structure" id="">
                            <option value="">Несущая конструкция</option>
                            @foreach($main_structures as $main_structure)
                                <option value="{{ $main_structure['id'] }}">{{ $main_structure['name'] }}</option>
                            @endforeach
                        </select>
                        <label for="wall_material"><p>Материал стен</p></label>
                        <select name="wall_material" id="">
                            <option value="">Материал стен</option>
                            @foreach($wall_materials as $wall_material)
                                <option value="{{ $wall_material['id'] }}">{{ $wall_material['name'] }}</option>
                            @endforeach
                        </select>
                        <div class="info_wrap">
                            <label for="parking"><p>Парковка</p></label>
                            <input type="button" value="Добавить" class="add_parking_field">
                        </div>
                        
                        <div id="parking_block">
                            <div class="info_wrap">
                                <select name="parking_type[]" id="">
                                    <option value="">Тип парковки</option>
                                    @foreach($parking_types as $parking_type)
                                        <option value="{{ $parking_type['id'] }}">{{ $parking_type['name'] }}</option>
                                    @endforeach
                                </select>
                                <input type="text"  name="parking_count[]" placeholder="000">
                                
                            </div>
                            
                        </div>
                        
                        
                        <label for="heating_type"><p>Тип отопления</p></label>
                        <select name="heating_type" id="">
                            <option value="">тип отопления</option>
                            @foreach($heating_types as $heating_type)
                                <option value="{{ $heating_type['id'] }}">{{ $heating_type['name'] }}</option>
                            @endforeach
                        </select>
                    </div><!-- info -->
                    
                
            </div>  <!-- info_wrap --> 
            
                    <label for="content_block"><p>описание</p></label>
                    <div class="text_editor">
                        <textarea name="editor1" class="text_editor" id="editor1" rows="10" cols="80"></textarea>
                    </div>
                <input type="submit" id="save_housing_estate" class="workspace_save" value="Сохранить жилой комплекс">
            </form>
        </div>  <!-- tab1 -->
        
        
        
       
    </div>    <!-- housing_complex -->   
</div> <!-- workspace -->
   <script>
 CKEDITOR.replace( 'editor1' );
 </script>
    <script src= "{{ URL::asset('js/ymaps.js') }}">
        // скрипт вывода маркера на карту на странице добавления объекта новостройки
    </script>
@endsection


<!--Добавить поле description в таблицу жилых комплексов

!!!! Жилой комплекс сдан\не сдеан-->