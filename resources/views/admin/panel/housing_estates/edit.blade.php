@extends('admin.layouts.panel')
@section('content')
<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::asset('/js/ckeditor/ckeditor.js')}}"></script>
<div class="button-panel">
    <a href="/manager/housing_estates">Список ЖК</a>
    <hr>
    <a href="/manager/housing_estate/{{$housing_estate->id}}/create_house">Добавить дом</a>
</div>
<div class="workspace">
    <div class="housing_complex">
        <div class="tabs">
            <label for="for_tab1" class="tab1 tab_button">Информация о ЖК </label>
            <label for="for_tab2" class="tab2 tab_button">Список домов </label>
            <label for="for_tab3" class="tab3 tab_button">Галерея</label>
            <label for="for_tab4" class="tab4 tab_button">Мета теги</label>
        </div>
        <div class="for_tab1">
            <form action="/manager/housing_estates/{{ $housing_estate->id}}" method="POST">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="updater" value="{{$updater_id}}">
		<input type="hidden" id="housing_estate_id" name='id' value="{{ $housing_estate->id}}">
            <div class="info_wrap">
                
                    <div class="info">
                        <label for="name"><p>Название ЖК</p></label>
                        <input type="text" name="name" value="{{$housing_estate->name }}"/>
<!--                        <label for="alias"><p>Псевдоним</p></label>
                        <input type="text" name="alias" value="{{$housing_estate->alias }}">-->
			<div class="agent_button">
			    <button id="add_agent_show">Добавить застройщика / Продавца</button>
			    <button id="add_agent_hide">Скрыть блок добавления застройщика / Продавца</button>
			</div>
                        
                        <div class="info_wrap agent_block" >
                            <label for="developer"><p>Застройщик:</p> </label>
                            <select name="agents[]"  class = "agent" id="">
                                <option value="">Выберите застройщика</option>
				@foreach($builders as $builder)
				<option value="{{$builder->id}}">{{$builder->name}}</option>
				@endforeach
                            </select>
			</div>
			<div class="info_wrap agent_block">
                        <label for="seller">Продавец: </label>
                        <select name="agents[]" class = "agent" id="">
                            <option value="">Выберите продавца</option>
                            @foreach($sellers as $seller)
				<option value="{{$seller->id}}">{{$seller->name}}</option>
				@endforeach
                        </select>
                        </div>
                        <div class="estate_location">
                            <label for="estate_location">Расположение:</label>
                            <select name="region" id='location_region'>
                                <option value="">Выберите регион</option>
                                @foreach($location_regions as $location_region)
                                    @if($housing_estate->location_region_id == $location_region['id'])
                                        <option value="{{ $location_region['id'] }} " selected>{{ $location_region['name'] }}</option>
                                    @else
                                        <option value="{{ $location_region['id'] }} ">{{ $location_region['name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <select name="location_area" id="location_area">
                                <option value="">Выберите регион</option>
                                @foreach($location_areas as $location_area)
                                    @if($housing_estate->location_area == $location_area['id'])
                                        <option value='{{ $location_area['id']}}' selected>{{$location_area['name']}}</option>
                                    @else
                                        <option value='{{ $location_area['id']}}'>{{$location_area['name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <label for="address"><p>Адрес</p></label>
                        <input type="text" name="address" value="{{ $housing_estate->address }}">
                        <hr>
                        <label for=""><p>Карта</p></label>
			    <p>Широта: 
				<input type="text" name="latitude" id="latitude"
				@if(!empty($housing_estate->latitude)) 
				    value="{{$housing_estate->latitude}}"
				@endif
				>
			    Долгота: <input type="text" name="longitude" id="longitude" 
			    @if(!empty($housing_estate->longitude)) 
				    value="{{$housing_estate->longitude}}"
				@endif
				></p>
                        <div class="complex_map" id="map" style="width: 500px; height: 400px">     
                        </div> 
                    </div> <!-- info -->
                    <div class="info">
                        <div class="published_radio">
                            <label for="published"><p>Опубликовано</p></label>
                            @if($housing_estate->published == 1 )
				<input type="radio" name="published" value="1" checked="true">Да
				<input type="radio" name="published" value="0">Нет
                            @else
				<input type="radio" name="published" value="1">Да
				<input type="radio" name="published" value="0" checked="true">Нет
                            @endif
                        </div>
                        <label for="start_construct">
                            <p>Начало строительства:</p>
                        </label>
                        <select name="start_construct" id="start_construct_date">
                            <option value="">Выберите дату</option>
                            @foreach($construct_dates as $construct_data)
                                @if($housing_estate->start_construct == $construct_data['id'])
                                    <option value="{{ $construct_data['id'] }}" selected>{{ $construct_data['name'] }}</option>
                                @else
                                    <option value="{{ $construct_data['id'] }}">{{ $construct_data['name'] }}</option>
                                @endif
                            @endforeach
                        </select>
                        <div class="info_wrap">
                            <div class="info">
                                <label for="end_construct">Срок сдачи</label>
                                <select name="end_construct" id="end_construct_data">
                                    <option value="">Срок сдачи</option>
                                    @foreach($construct_dates as $construct_data)
                                        @if($housing_estate->end_construct == $construct_data['id'])
                                            <option value="{{ $construct_data['id'] }}" selected>{{ $construct_data['name'] }}</option>
                                        @else
                                            <option value="{{ $construct_data['id'] }}">{{ $construct_data['name'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <label for="count_houses"><p>Количество домов</p></label>
                        <input type="text" name="count_houses" value="{{ $housing_estate->count_houses }}">
                        <label for="count_apartments"><p>Количество квартир в ЖК</p></label>
                        <input type="text" name="count_apartments" value="{{ $housing_estate->count_apartments }}">
                        <label for="main_structure"><p>Несущая конструкция</p></label>
                        <select name="main_structure" id="">
                            <option value="">Несущая конструкция</option>
                            @foreach($main_structures as $main_structure)
                                @if($housing_estate->main_structure == $main_structure['id'])
                                    <option value="{{ $main_structure['id'] }}" selected>{{ $main_structure['name'] }}</option>
                                @else
                                    <option value="{{ $main_structure['id'] }}">{{ $main_structure['name'] }}</option>
                                @endif
                            @endforeach
                        </select>
                        <label for="wall_material"><p>Материал стен</p></label>
                        <select name="wall_material" id="">
                            <option value="">Материал стен</option>
                            @foreach($wall_materials as $wall_material)
                                @if($housing_estate->wall_material == $wall_material['id'])
                                <option value="{{ $wall_material['id'] }}" selected>{{ $wall_material['name'] }}</option>
                                @else
                                    <option value="{{ $wall_material['id'] }}">{{ $wall_material['name'] }}</option>
                                @endif
                            @endforeach
                        </select>
                        <div class="info_wrap">
                            <label for="parking"><p>Парковка</p></label>
                            <input type="button" value="Добавить" class="add_parking_field">
                        </div>
                        
                        <div id="parking_block">
                            <div class="info_wrap">
                                <select name="parking_type[]" id="">
                                    <option value="">Тип парковки</option>
                                    @foreach($parking_types as $parking_type)
                                        <option value="{{ $parking_type['id'] }}">{{ $parking_type['name'] }}</option>
                                    @endforeach
                                </select>
                                <input type="text"  name="parking_count[]" placeholder="000"> 
                            </div>
			    <hr>
			    @if($parking_arr)
			    @foreach($parking_arr as $parking)
			    <div class="info_wrap">	
				<select name="parking_type[]" id="">
				    @foreach($parking_types as $parking_type)
					@if($parking->parking_id == $parking_type['id'])
					    <option value="{{ $parking_type['id'] }} "selected>{{ $parking_type['name'] }}</option>
					@else
					    <option value="{{ $parking_type['id'] }}">{{ $parking_type['name'] }}</option>
					@endif    
				    @endforeach
				</select>
				<input type="text"  name="parking_count[]" value='{{ $parking->count }}'>
			    </div>
			    @endforeach
			    @endif
                        </div>
                        <label for="heating_type"><p>Тип отопления</p></label>
                        <select name="heating_type" id="">
                            <option value="">тип отопления</option>
                            @foreach($heating_types as $heating_type)
                                @if($housing_estate->heating_type == $heating_type['id'])
                                <option value="{{ $heating_type['id'] }}" selected>{{ $heating_type['name'] }}</option>
                                @else
                                    <option value="{{ $heating_type['id'] }}">{{ $heating_type['name'] }}</option>
                                @endif
                            @endforeach
                        </select>
			<label for="builder">Список застройщиков</label>
			<table>
			    @foreach($agents_arr as $agent)
				@if($agent->type_agent == 1)
				<tr>
				    <td><img style="width: 50px"src="/storage/{{$agent->logo_src}}"></td>
				    <td>{{$agent->name}}</td>
				    <td>{{$agent->site}}</td>
				</tr>
				     <br>
				@endif
			@endforeach
			</table>
			<label for="builder">Список продавцов</label>
			<table>
			    @foreach($agents_arr as $agent)
				@if($agent->type_agent == 2)
				<tr>
				    <td><img style="width: 50px"src="/storage/{{$agent->logo_src}}"></td>
				    <td>{{$agent->name}}</td>
				    <td>{{$agent->site}}</td>
				</tr>
				     <br>
				@endif
			@endforeach
			</table>
                    </div><!-- info -->
            </div>  <!-- info_wrap --> 
            
                    <label for="content_block"><p>описание</p></label>
                    <div class="text_editor">
                        <textarea name="description" class="text_editor" id="editor1" rows="10" cols="100">
@if(!empty($housing_estate->description))
{{ $housing_estate->description }}
@endif</textarea>
                    </div>
                {{ csrf_field() }}
                <input type="submit" id="save_housing_estate" class="workspace_save" value="Сохранить изменения">
            </form>
        </div>  <!-- tab1 -->
        <div class="for_tab2">
            <p>Список домов</p>
            <table class="house_list">                
                <tr>
                    <th>Номер</th>
		    <th>Название</th>
                    <th>Количество этажей</th>
                    <th>Количество подъездов</th>
                    <th>Количество квартир</th>
                    <th>Начало строительства</th>
                    <th>Срок сдачи</th>
                    <th>Материал стен</th>
                    <th>Наличие лифта</th>
                    <th>Количество парковок</th>
                    <th>Адрес</th>
		</tr>
		@foreach($houses as $k=>$house)
		<tr>
		    <td>{{++$k}}</td>
		    <td><a href="/manager/house/{{$house->id}}/edit">{{$house->name}}</a></td>
		    <td>{{$house->count_floors}}</td>
		    <td>{{$house->count_entranses}}</td>
		    <td>{{$house->count_apartments}}</td>
		    <td>{{$house->main_structure}}</td>
		    <td>{{$house->ceiling_height}}</td>
		    <td>{{$house->heating_type}}</td>
		</tr>
		@endforeach  
                </tr>
            </table>
        </div> <!-- for_tab2 -->
        <div class="for_tab3">
	    
            <div class="complex_logo">
		
		<!--<form action="/manager/housing_estates/{{$housing_estate->id}}/logo_update" enctype="multipart/form-data" method="POST">-->
		    <!--{{csrf_field() }}-->
		    <!--<input type="hidden" id="housing_estate_id" name='id' value="{{ $housing_estate->id}}">-->
		    <div class="info">
			<h3 class="logo_wrap_title">Логотип жилого комплекса</h3>
			<div class="housing_estate_logo">
			   <div class="logo">
			       <img src="/storage/{{$housing_estate->logo_src}}" 
				    alt="{{ $housing_estate->logo_description }}">
			   </div>
		    <input type="file" name="housing_estate_logo" id="input_housing_estate_logo" value="Загрузить">
		    <textarea name="logo_description" id="logo_descripton" rows="5" cols="50" >{{ $housing_estate->logo_description }}</textarea>
		    <input type="submit" id="update_housing_estate_logo" value="Сохранить" name="">
		    </div> 
			</div>
			
		    
		    
		<!--</form>-->
                
            </div>
            <hr>
            <div class="complex_gallery">
                <h3>Галлерея</h3>
		 <div class="gallery_img info_wrap sortable">
                    @foreach($housing_estate_gallery_images as $gallery_image)
		    <div class="info">
			<img src="/storage/{{$gallery_image->src}}" alt="{{$gallery_image->meta_description}}"
			     data-id="{{$gallery_image->id}}" data-order="{{$gallery_image->order}}">
			<label for="description">Описание</label>
			<input type="text" name="description" class="description" 
			       value="{{$gallery_image->description}}">
			<label for="meta_description">Мета описание</label>
			<input type="text" name="meta_description" class="meta_description" 
			       value="{{$gallery_image->meta_description}}">
			<div class="info_wrap">
			    <button class="delete_gallery_img">Удалить</button>
			    <button class="change_description">Обновить</button>
			</div>
		    </div>  
		    @endforeach   
                </div>
		<button class="change_order">Изменить порядок</button>
		
		
		
                <div class="gallery_img_button">
		    <h3>Добавить изображение в галерею</h3>
		    <div class="info">
			<input type="file" id="input_gallery_img" name="gallery_img">
			<label for="description">Описание</label>
			<input type="text" id="input_gallery_img_description" name="description">
			<label for="meta_description">Мета-описание</label>
			<input type="text" id="input_gallery_img_meta_description" name="meta_description">
			<input type="submit" id="add_gallery_image" value="Добавить">
		    </div>
                </div>
		
		
		
            </div> <!-- complex_gallery -->
        </div>
        <div class="for_tab4">
	    <h2>{{$housing_estate->name }}</h2>
	    <textarea name="" id="meta_title" cols="200" rows="10">{{$housing_estate->meta_title}}</textarea>
	    <p>Описание страницы</p>
	    <textarea name="" id="meta_description" cols="200" rows="10">{{$housing_estate->meta_description}}</textarea>
	    <input type="submit" id="meta_save_housing_estate" class="workspace_save" value="Сохранить изменения">
	    <div class="success_data"></div>
	</div>
    </div>    <!-- housing_complex -->   
</div> <!-- workspace -->
<script>
    /* инициализация визуального редактора */
 CKEDITOR.replace( 'editor1' );
 </script>
   <script type="text/javascript" src="{{ URL::asset('js/jquery-ui-1.12.1.custom/jquery-ui.js') }}">
   /*
    * скрипт изменения порядка изображений в галерее жилого комплекса
    */
   </script>
    <script type="text/javascript">
        $('document').ready(function(){
	    $( ".sortable" ).sortable({ tolerance:"intersect" }).disableSelection();
	    var token = '<?php echo csrf_token() ?>';
	    var housing_estate_id = $('#housing_estate_id').val();
            $('.for_tab2, .for_tab3, .for_tab4').hide();
                // показывает блок 1
            $('.tab1').click(function(){
                $('.for_tab2, .for_tab3, .for_tab4').hide();
                $('.for_tab1').show();
            });
                // показывает блок 2
            $('.tab2').click(function(){
                $('.for_tab1, .for_tab3, .for_tab4').hide();
                $('.for_tab2').show();
            });
                // показывает блок 3
            $('.tab3').click(function(){
                $('.for_tab1, .for_tab2, .for_tab4').hide();
                $('.for_tab3').show();
            });
                // показывает блок 4
            $('.tab4').click(function(){
                $('.for_tab1, .for_tab2, .for_tab3').hide();
                $('.for_tab4').show();
            });
                    // подгружает список районов при выборе региона
            $('#location_region').change(function(){
                var location_region_id = $('#location_region option:selected').val();
                
                $.ajax({
                    url: '/parameter/location/'+location_region_id,
                    type: 'POST',
                    data: {'_token': token},
                    success: function(data){
                        $('#message').html(data);
                    } 
                });
            });
                // добавляет поле для сохранения типа парковки
            $('.add_parking_field').click(function(){
                var parking_block = $('#parking_block > :first-child').html();
                //console.log(parking_block);
                $('#parking_block').append('<div class="info_wrap">'+parking_block+'</div>');
            });
		// показывает блок добавления застройщика/продавца
	    $('.agent_block').hide();
	    $('#add_agent_show').click(function(){
		$('.agent_block').show();
		$('#add_agent_show').hide();
		$('#add_agent_hide').show();
		return false;
	    });
	    $('#add_agent_hide').click(function(){
		$('.agent_block').hide();
		$('#add_agent_hide').hide();
		$('#add_agent_show').show();
		return false;
	    });
	    $('#meta_save_housing_estate').click(function(){
		var title = $('#meta_title').val();
		var description = $('#meta_description').val();
		console.log(title+'---'+description);
		// переменные токен и id жилого комплекса определены в начале скрипта
		var url = '/manager/housing_estates/'+housing_estate_id+'/meta_update';
		var data = {'meta_title':title, 'meta_description':description, '_token':token};
		$.ajax({
		    url: url,
		    method: 'POST',
		    data: data,
		    success: function(data){
			$('.success_data').html('<p>'+data+'</p>').delay('5000');
			$('.success_data').hide(1000);
		    }
		});
		return false;
	    });

		// по клику на кнопку удалить - удаляем картинку хода строитесльва id картинки
		// которую нужно удалить передаем в url 
		// если удаление успешно, удаляем блок с этой картинкой 
	    $('body').on('click', '.delete_gallery_img', function(){
		var wrap = $(this).parent('.info_wrap').parent('.info');
		var image_id = $(this).parent().siblings('img').data('id');

		$.post(
		    '/manager/'+housing_estate_id+'/delete_gallery_image/'+image_id,
		    { _token:token },
		    function(data){
			if(data === 0){
			    $('#message').show().html('Ошибка удаления изображения');
			    $('#message').delay(2000).hide(500).html('');
			}else{
			    $('#message').show().html('Успешно');
			    $('#message').delay(2000).hide(500).html();
			    wrap.remove();
			}
		    }
		);
	    });

		// по клику на кнопку обновить получаем данные из полей описание 
		// и мета-описание текущей картинки, ее id и сохраняем в БД
	    $('.change_description').click(function(){
		var description = $(this).parent().siblings('.description').val();
		var meta_description = $(this).parent().siblings('.meta_description').val();
		var image_id = $(this).parent().siblings('img').data('id');
		var data = {'description':description, 'meta_description':meta_description, '_token':token};
		
		$.ajax({
		    url: '/manager/'+housing_estate_id+'/gallery_image/'+image_id,
		    type: 'POST',
		    data:data,
		    success: function(data){
			$('#message').show(500).html(data);
			$('#message').delay(2000).hide(500);		    
		    }
		});
	    });

		// по клику на кнопку обновить порядок проходим по каждой картинке
		// в блоке галереи и изменяем параметр data-order начиная 
		// с 1, потом сохраняем обновляенный порядок картинок в БД
	    $('.change_order').click(function(){
		var data_array = [];
		$(this).siblings('.info_wrap').children('.info').children('img').each(function(e){
		    $(this).data('order', e+1);
		    var id = $(this).data('id');
		    var order = $(this).data('order');
		    data_array[e] = {'id':id, 'order':order};
		    console.log(data_array);
		});
		
		var token = '<?php echo csrf_token() ?>';
		var data = {'_token':token, 'gallery_images':data_array};

		$.ajax({
		    url: '/manager/'+housing_estate_id+'/change_gallery_images_order',
		    type: 'POST',
		    data: data,
		    success: function(data){
			$('#message').show(500).html(data);
			$('#message').delay(2000).hide(500);
		    }
		});
		return false;
	    });
		
		// добавляем картинку в галерею жилого комплекса
	    $('#add_gallery_image').click(function(){
		var gallery_img = $('#input_gallery_img').prop('files')[0];
		var description = $('#input_gallery_img_description').val();
		var meta_description = $('#input_gallery_img_meta_description').val();
console.log(gallery_img);		
		var imageData = new FormData();
		imageData.append('gallery_img', gallery_img);
		imageData.append('_token', token);
		imageData.append('description', description);
		imageData.append('meta_description', meta_description);
		
		    // отправляем запрос на сервер с id жилого комплекса
		$.ajax({
		    url: '/manager/housing_estates/'+housing_estate_id+'/add_gallery_img',
		    dataType: 'JSON',
		    cache: false,
		    contentType: false,
		    processData: false,
		    data: imageData,
		    type: 'POST',
		    success: function(data){
			if(data === 0){
			    $('#message').show().html('Ошибка добавления изображения');
			    $('#message').delay(2000).hide(500).html('');
			}else{
			    $('#input_gallery_img').val('');
			    $('#message').show().html('Успешно');
			    $('#message').delay(2000).hide(500).html();
				// сформировать блок с картинкой и добавить в список
			    var div = '<div class="info">'+
				'<img src="/storage/'+data.src+'" alt="'+data.meta_description+'" '+
				'data-id="'+data.id+'" data-order="'+data.order+'">'+
				'<label for="description">Описание</label>'+
				'<input type="text" name="description" class="description"'+
				'value="'+ data.description +'">'+
				'<label for="meta_description">Мета описание</label>'+
				'<input type="text" name="meta_description" class="meta_description"'+
				'value="'+ data.meta_description +'">'+
				'<div class="info_wrap">'+
				    '<button class="delete_gallery_img">Удалить</button>'+
				    '<button class="change_description">Обновить</button>'+
				'</div>'+
			    ' </div> ';
			    $('.gallery_img').append(div);
			}
		    }
		});
		return false;
	    });
	    
		// по клику на кнопку обновить логотип получаем id комплекса
		// токен, картинку и отправляем на сервер 
	    $('#update_housing_estate_logo').click(function(){
		var logo = $('#input_housing_estate_logo').prop('files')[0];
		var logo_description = $('#logo_descripton').val();
		var logoData = new FormData();
		logoData.append('housing_estate_logo', logo);
		logoData.append('_token', token);
		logoData.append('logo_description', logo_description);

		    // отправляем запрос на сервер с id жилого комплекса
		$.ajax({
		    url: '/manager/housing_estates/'+housing_estate_id+'/logo_update',
		    dataType: 'JSON',
		    cache: false,
		    contentType: false,
		    processData: false,
		    data: logoData,
		    type: 'POST',
		    success: function(data){
			if(data === 0){
			    $('#message').show().html('Ошибка обновления логотипа');
			    $('#message').delay(2000).hide(500).html('');
			}else{ 
			    $('#message').show().html('Успешно');
			    $('#message').delay(2000).hide(500).html();
			    $('.logo').html('');
			    $('#input_housing_estate_logo').val('');
			    $('#logo_descripton').val(data.logo_description);
			    $('.logo').append('<img src="/storage/'+data.logo_src+'">');
	
			}
		    }
		});
		return false;
	    });
        }); // document
    </script>
    <script>
        var coordinates = new Array();
        coordinates = [<?php echo  $housing_estate->latitude; ?>, <?php echo  $housing_estate->longitude; ?>];
    </script>
    <script src= "{{ URL::asset('js/ymaps_edit.js') }}">
        // скрипт вывода маркера на карту на странице добавления объекта новостройки
    </script>
@endsection