@extends('admin.layouts.panel')
@section('content')
<div class="button-panel">
<a href="/manager/seller/create">Создать</a> 
</div>
<div class="workspace">
    <h1>Список продавцов</h1>
    <table>
	<tr>
	    <th>№</th>
	    <th>Логотип</th>
	    <th>Название</th>
	    <th>Адрес</th>
	    <th>Телефон</th>
	    <th>Email</th>
	    <th>сайт</th>
	    <th>опубликован</th>
	</tr>
	@foreach($sellers as $k => $seller)
	<tr>
	    <td></td>
	    <td><img style="width: 100px;" src="{{ URL::asset('storage/'.$seller->logo_src) }}" alt='логотип продавца'</td>
	    <td><a href="/manager/seller/{{$seller->id}}/edit" >{{$seller->name}}</a></td>
	    <td>{{$seller->email}}</td>
	    <td>{{$seller->address}}</td>
	    <td>{{$seller->phone}}</td>
	    <td>{{$seller->logo_src}}</td>
	    <td>{{$seller->published}}</td>
	</tr>
	@endforeach
	<tr>
	    <td></td>
	</tr>
    </table>    
</div>
@endsection