@extends('admin.layouts.panel')
@section('content')
<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::asset('/js/ckeditor/ckeditor.js')}}"></script>
<div class="button-panel">
<a href="/manager/seller">К списку </a>
    
</div>
<div class="workspace">
    <h1>{{ $seller->name }}</h1>
    <form action="/manager/seller/{{$seller->id}}" enctype="multipart/form-data" method="POST">
	<input type="hidden" name="type_agent" value="1">
	<input type="hidden" name="_method" value="PUT">
	{{csrf_field()}}
    <div class="info_wrap">
	    
	<div class="info">
	    <label for="logo">Логотип:</label>
	    @if(!$seller->logo_src)
		<input type="file" name="logo_src" accept="">
		<label for="logo_description">Название логотипа</label>
		<input type="text" name="logo_description" value="{{$seller->logo_description}}">
	    @else
	    <img class="logo_img" src="{{ URL::asset('/storage/'.$seller->logo_src) }}" alt="{{$seller->logo_description}}">
	    <input type="hidden" name="logo_src" value="{{ $seller->logo_src }}" >
	    <label for="logo_description">Название логотипа</label>
	    <input type="text" name="logo_description" value="{{$seller->logo_description}}">
	    @endif
		
	    <label for=""><p>Карта</p></label>
	    <p>Широта: <input type="text" name="latitude" id="latitude" value="{{$seller->latitude}}">
	    Долгота: <input type="text" name="longitude"  id="longitude" value="{{$seller->longitude}}"></p>
	    <div class="complex_map" id="map" style="width: 500px; height: 400px">     
	    </div> 
	</div>
	
	<div class="info">
	    <div class="info_wrap">
		<label for="published">Опубликовано</label>
		@if( $seller->published == 0 )
		    <input type="radio" name="published" value="0" checked="checked">Нет
		    <input type="radio" name="published" value="1">Да
		@else
		    <input type="radio" name="published" value="0"  >Нет
		    <input type="radio" name="published" value="1" checked="checked" >Да
		@endif

	    </div>
	    <label for="name">название</label>
	    <input type="text" name="name" value="{{$seller->name}}">
	    <label for="address">Адрес</label>
	    <input type="text" name="address" value="{{$seller->address}}">
	    <label for="phone">телефон</label>
	    <input type="text" name="phone" value="{{$seller->phone}}">
	    <label for="email">Email</label>
	    <input type="text" name="email" value="{{$seller->email}}">
	    <label for="site">Сайт</label>
	    <input type="text" name="site" value="{{$seller->site}}">
	</div>
	    
	
	
	
    </div> <!-- info_wrap -->
    <div class="info">
	<label for="description">описание</label>
	<textarea name="description" id="editor1" rows="10" cols="200">{!! $seller->description !!}</textarea>
    </div>
	<input type="submit" id="save_seller" class="workspace_save" value="Сохранить изменения">
    </form>
</div> <!--	workspace -->
<script>
        /* инициализация визуального редактора */
    CKEDITOR.replace( 'editor1' );
</script>
<script type="text/javascript">
    (function($, undefined){
	$(function(){
	    $('.logo_img').dblclick(function(){
		$(this).parent('.info').prepend(
		    '<input type="file" name="logo_src" accept="">'
		);
	    });
	});
})(jQuery);
    </script>

<script>
        var coordinates = new Array();
        coordinates = [<?php echo  $seller->latitude; ?>, <?php echo  $seller->longitude; ?>];
       
       
    </script>
    
    
<script src= "{{ URL::asset('js/ymaps_edit.js') }}">
@endsection