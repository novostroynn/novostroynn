@extends('admin.layouts.panel')
@section('content')
<div id="apartment_plain_modal">
	<div class="apartment_plain_wrap">
	     <div id="modal_msg"></div>
	    <button class="close_plain_modal">Закрыть окно</button>
	    <div class="info_wrap">
		<div class="info input_plain">
		    {{csrf_field()}}
		    <input type="hidden" class="apartment_id" value="">
		    <input name="apartment_plain" type="file" id="input_apartment_plain">
		    <button id="add_apartment_plain">Сохранить</button>
		</div>
		<button id="change_plain_order">Изменить порядок</button>
	    </div>
	    <div class="info_wrap" id="plain_collection">
		
	    </div>
	</div>
    </div>
<div class="button-panel">
    <a href="/manager/housing_estates/{{$house->housing_estate_id}}/edit">Назад</a>
    <hr>
    <a href="/manager/house/{{$house->id}}/house_progress">Ход строительства</a>
</div>
<div class="workspace">
    <h2>Страница редактирования дома</h2>
    <form action="/manager/house/{{$house->id}}" method="POST">
	<input type="hidden" name="_method" value="PUT">
	{{csrf_field()}}
    <div class="info_wrap">
	    <label for="published">Опубликован</label>
	    <div class="info_wrap">
		@if($house->published == 1)
		    <input type="radio" name="published" value="0" >нет
		    <input type="radio" name="published" value="1" checked> да
		@else
		    <input type="radio" name="published" value="0" checked>нет
		    <input type="radio" name="published" value="1"> да
		@endif
	    </div>
    </div>
    <div class="info_wrap">
	<div class="info">
	    <label for="name">Название дома</label>
	    <input type="text" name="name" value="{{$house->name}}">
	    <label for="count_floors">Количество этажей</label>
	    <input type="text" name="count_floors" value="{{$house->count_floors}}">
	    <label for="count_entranses">Количество подъездов</label>
	    <input type="text" name="count_entranses" value="{{$house->count_entranses}}">
	    <label for="count_apartments">Количество квартир</label>
	    <input type="text" name="count_apartments" value="{{$house->count_apartments}}">
	    <label for="ceiling_height">Высота потолков</label>
	    <input type="text" name="ceiling_height" value="{{$house->ceiling_height}}">  
	</div> <!-- info -->
	<div class="info">
	   <label for="start_construct">Начало строительства</label>
	    <select name="start_construct">
		<option value="">Начало строительства</option>
		@foreach($construct_dates as $construct_data)
		    @if($house->start_construct == $construct_data->id)
		    <option value="{{$construct_data->id}}" selected>{{$construct_data->name}}</option>
		    @else
		    <option value="{{$construct_data->id}}">{{$construct_data->name}}</option>
		    @endif
		@endforeach
	    </select>
	    <label for="end_construct">Срок сдачи</label>
	    <select  name="end_construct">
		<option value="">Срок сдачи</option>
		@foreach($construct_dates as $construct_data)
		    @if($house->end_construct == $construct_data->id)
		    <option value="{{$construct_data->id}}" selected>{{$construct_data->name}}</option>
		    @else
		    <option value="{{$construct_data->id}}">{{$construct_data->name}}</option>
		    @endif
		@endforeach
	    </select>
	    <label for="main_structure">Несущая конструкция</label>
	    <select name="main_structure">
		<option value="">Несущая конструкция</option>
		@foreach($main_structures as $main_structure)
		    @if($house->main_structure == $main_structure->id)
		    <option value="{{$main_structure->id}}" selected>{{$main_structure->name}}</option>
		    @else
		    <option value="{{$main_structure->id}}">{{$main_structure->name}}</option>
		    @endif
		@endforeach
	    </select>
	    <label for="wall_material">Материал стен</label>
	    <select name="wall_material">
		<option value="">Материал стен</option>
		@foreach($wall_materials as $wall_material)
		    @if($house->wall_material == $wall_material->id)
		    <option value="{{$wall_material->id}}" selected>{{$wall_material->name}}</option>
		    @else
		    <option value="{{$wall_material->id}}">{{$wall_material->name}}</option>
		    @endif
		@endforeach
	    </select>
	    <label for="apartment_finifing">Отделка квартир</label>
	    <select name="apartment_finifing">
		<option value="">Отделка квартир</option>
		@foreach($apartment_finishings as $apartment_finishing)
		    @if($house->apartment_finishing == $apartment_finishing->id)
		    <option value="{{$apartment_finishing->id}}" selected>{{$apartment_finishing->name}}</option>
		    @else
		    <option value="{{$apartment_finishing->id}}">{{$apartment_finishing->name}}</option>
		    @endif
		@endforeach
	    </select> 
	</div> <!-- info -->
	<div class="info">
	    <label for="heating_type">Тип отопления</label>
	    <select name="heating_type" id="">
		<option value="">Тип отопления</option>
		@foreach($heating_types as $heating_type)
		    @if($house->heating_type == $heating_type->id)
		    <option value="{{$heating_type->id}}" selected>{{$heating_type->name}}</option>
		    @else
		    <option value="{{$heating_type->id}}">{{$heating_type->name}}</option>
		    @endif
		@endforeach
	    </select>
	    <label for="cooker_type">Тип кухонной плиты</label>
	    <select name="cooker_type" id="">
		<option value="">Тип кухонной плиты</option>
		@foreach($cooker_types as $cooker_type)
		    @if($house->cooker_type == $cooker_type->id)
		    <option value="{{$cooker_type->id}}" selected>{{$cooker_type->name}}</option>
		    @else
		    <option value="{{$cooker_type->id}}">{{$cooker_type->name}}</option>
		    @endif
		@endforeach
	    </select>
	    <label for="window_installation">Тип остекления</label>
	    <select name="window_installation" id="">
		<option value="">Материал остекления</option>
		@foreach($window_installations as $window_installation)
		    @if($house->window_installation == $window_installation->id)
		    <option value="{{$window_installation->id}}" selected>{{$window_installation->name}}</option>
		    @else
		    <option value="{{$window_installation->id}}">{{$window_installation->name}}</option>
		    @endif
		@endforeach
	    </select>
	    <label for="build">Сдан</label>
	    <div class="info_wrap">
		@if($house->build == 1)
		    <input type="radio" name="build" value="0" >не сдан
		    <input type="radio" name="build" value="1" checked> сдан
		@else
		    <input type="radio" name="build" value="0" checked>не сдан
		    <input type="radio" name="build" value="1"> сдан
		@endif
	    </div>
	</div>
    </div> <!-- info_wrap -->
    <input type="submit" id="save_house" class="workspace_save" value="Сохранить изменения">
    </form>
    <div class="info_wrap">    
	<div class="info">
	    <p>Планировки дома</p>
	    <div class="house_plain_wrap" id="sortable">
		@foreach($house_plains as $house_plain)
		<div class="plain">
		    <img src="/storage/{{$house_plain->src}}" alt="{{$house_plain->meta_description}}" 
		    data-id="{{$house_plain->id}}" data-order="{{$house_plain->order}}"
		    class="house_plain_img">
		    <label for="description">Описание:</label>
		    <input type="text" name="description" class="description" value="{{$house_plain->description}}">
		    <label for="meta_description">Мета-описание:</label>
		    <input type="text" name="meta_description" class="meta_description" value="{{$house_plain->meta_description}}">
		    <div class="success_description"></div>
		    <div class="info_wrap">
			<button class="delete_plain">Удалить</button>
			<button class="change_description">Обновить</button>
		    </div>
		</div>
		
		@endforeach
	    </div>
	    <div class="success"></div>
	    <button id="change_order">Обновить</button>
	    <hr>
	    <div class="add_plain">
		<p>Добавить планировку</p>
		<form action="/manager/{{$house->id}}/save_plain" enctype="multipart/form-data" method="POST">
		    {{csrf_field()}}
		    <input type="hidden" id="house_id" name="house_id" value="{{$house->id}}">
		    <input type="hidden" name="order" value="0">
		    <div class="info">
			<input type="file" name="house_plain" id="plain">
			<label for="description">Описание</label>
			<input type="text" name="description">
			<label for="meta_description">мета описание</label>
			<input type="text" name="meta_description">
			<input type="submit" value="Отправить">
		    </div>
		</form>
	    </div>
	</div>   
    </div> 
    <h3 class="title_appartment_table">Таблица квартир</h3>
	<form action="/manager/house/{{$house->id}}/update_apartments" method="POST">
	    {{csrf_field()}}
	<table class="apartment_lists">
	    <tr>
		<th>Тип квартиры</th>
		<th>Этаж</th>
		<th>S общая</th>
		<th>S жилая</th>
		<th>S кухни</th>
		<th>Отделка</th>
		<th>Цена</th>
		<th>Планировка</th>
		<th>Удалить</th>
	    </tr>
	    @foreach($apartments as $apartment)
	 
		<tr>
		    <input type="hidden" name="apartment_id[]" value="{{$apartment->id}}" class="apartment_id">   
		    <td>
			<select name="apartment_type[]" >
			    @foreach($apartment_types as $apartment_type)
				@if($apartment_type->id == $apartment->apartment_type)
				    <option value="{{$apartment_type->id}}" selected>{{$apartment_type->name}}</option>
				@else
				    <option value="{{$apartment_type->id}}">{{$apartment_type->name}}</option>
				@endif
			    @endforeach
			</select>
		    </td>
		    <td>
			<input type="text" name="apartment_floor[]" value="{{$apartment->floor}}">
		    </td>
		    <td><input type="text" name="apartment_total_area[]" value="{{$apartment->total_area}}"></td>
		    <td><input type="text" name="apartment_living_area[]" value="{{$apartment->living_area}}"></td>
		    <td><input type="text" name="apartment_kitchen_area[]" value="{{$apartment->kitchen_area}}"></td>
		    <td>
			<select name="apartment_finishing[]">
			    @foreach($apartment_finishings as $apartment_finishing)
				@if($apartment->finishing_type == $apartment_finishing->id)
				    <option value="{{$apartment_finishing->id}}" selected>{{$apartment_finishing->name}}</option>
				@else
				    <option value="{{$apartment_finishing->id}}">{{$apartment_finishing->name}}</option>
				@endif
			    @endforeach
			</select>
		    </td>
		    <td><input type="text" name="apartment_price[]" value="{{$apartment->price}}"></td>
		    <td><button class="apartment_plain">Планировка</button></td>
		    <td><button class="delete_apartment">Del</button></td>
		</tr>
	    @endforeach
	    
	</table>
	    <button class="update_apartments">Сохранить таблицу</button>
	</form>
    <h3 id="add_table_string" class="title_appartment_table">Добавить квартиру</h3>
    <form action="/manager/{{$house->id}}/add_apartments" method="POST">
	{{csrf_field()}}
    <table class="add_apartments">
	<tr>
	    <th>Тип квартиры</th>
		<th>Этаж</th>
		<th>S общая</th>
		<th>S жилая</th>
		<th>S кухни</th>
		<th>Отделка</th>
		<th>Цена</th>
		<th>Удалить</th>
	</tr>
    </table>
    <button id="save_new_apartment_string">Сохранить квартиры</button>
    </form>
</div> <!-- workspace -->
<script type="text/javascript" src="{{ URL::asset('js/sortable.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery-ui-1.12.1.custom/jquery-ui.js') }}"></script>

<script type="text/javascript">
$('document').ready(function(){
    var token = '<?php echo csrf_token() ?>';
//    добавляет строку с типом квартиры
    $('#add_table_string').click(function(){
	
	
	var apartment_type_select = '<?php 
	    echo '<select name="apartment_type[]" class="type" required > ';
	    echo '<option value="">Тип квартиры</option>';
		foreach($apartment_types as $apartment_type){
		    echo "<option value=\'$apartment_type->id\'>$apartment_type->name</option> "; 
		} 
	    echo '</select>'; 
	    ?>';
	var apartment_finishing_type = '<?php 
	    echo '<select name="apartment_finishing[]" class="finishing" required >';
	    echo '<option value="">Тип отделки</option>';
		foreach($apartment_finishings as $apartment_finishing){
		    echo "<option value=\'$apartment_finishing->id\'>$apartment_finishing->name</option> "; 
		} 
	    echo '</select>'; 
	    ?>';

	var tableString = '<tr class="apartment_string"><td>'+apartment_type_select+'</td>'+
		'<td><input type="text" name="apartment_floor[] "class="floor">'+'</td>'+
		'<td><input type="text" name="apartment_total_area[] class="total_area"></td>'+
		'<td><input type="text" name="apartment_living_area[]" class="living_area"></td>'+
		'<td><input type="text" name="apartment_kitchen_area[]" class="kitchen_area"></td>'+
		'<td>'+apartment_finishing_type+'</td>'+
		'<td><input type="text" name="apartment_price[]" class="price"></td>'+
		'<td><button class="delete_new_apartment_string">Удалить</button></td>'
		'</tr>';

	$('.add_apartments').append(tableString);
	return false;
    });
//	удаляет строку с типом квартиры
    $('.apartment_lists').on('click','.del_table_string', function(){
	$(this).parent().parent().remove();
	return false;
    });
    
    $('#add_house_plain').click(function(){
	var file = $('#plain').files;
	console.log(file);
	return false;
    });
	
	// изменяет порядок расположения картинок планировок дома
    $( "#sortable" ).sortable({ tolerance:"intersect" }).disableSelection();
    
    $('#change_order').click(function(){
	var data_array = [];
	    // проходим по каждому элементу и собираем данные
	$('#sortable .plain img').each(function(e){  
	    $(this).data('order', e+1);
	    var id = $(this).data('id');
	    var order = $(this).data('order');
	    data_array[e] = {'id':id, 'order':order};
	});
//	var token = '<?php //echo csrf_token() ?>';
	var house_id = $('#house_id').val();
	var data = {'_token':token, 'images_arr':data_array};
	$.ajax({
	    url: '/manager/'+house_id+'/change_plain_order',
	    type: 'POST',
	    data: data,
	    success: function(data){
		$('.success').html(data);
		$('.success').delay(2000).hide(1000);
	    }
	});
	return false;
    });
    
	// обновляет название и описание планировки дома
    $('.change_description').click(function(){
	var msg = $(this).parent().siblings('.success_description');
	var description = $(this).parent().siblings('.description').val();
	var meta_description = $(this).parent().siblings('.meta_description').val();
	
	var house_id = $('#house_id').val();
	var image_id  = $(this).parent().siblings('.house_plain_img').data('id');
	var data = {'description':description, 'meta_description':meta_description, '_token':token};
	
	$.ajax({
	    url: '/manager/'+house_id+'/edit_plain/'+image_id,
	    type: 'POST',
	    data: data,
	    success: function(data){
		msg.html(data).show(800);
		msg.delay(2000).hide(1000);
	    }
	});
	return false;
    });
    
	// удаляет планировку дома
    $('.delete_plain').click(function(){
	var msg = $(this).parent().siblings('.success_description');
	var image_id = $(this).parent().siblings('.house_plain_img').data('id');
	//var token = '<?php //echo csrf_token() ?>';
	var house_id = $('#house_id').val();

	$.post(
	    '/manager/'+house_id+'/delete_plain_image/'+image_id,
	    { _token:token },
	    function(data){
		if(data > 0){
		    location.reload();
		}else{
		    msg.text('<p>Ошибка удаления</p>');
		}
	    }
	);
	return false;
    });
    
    $('body').on('click', '.delete_new_apartment_string', function(){
	$(this).parent().parent().remove();
	return false;
    });
    
	// показывает модальное окно с планировками по id квартиры
	// предает в модальное окно id квартиры
	// выводит планировки этой квартиры
    $('.apartment_plain').click(function(){
	var apartmentId = $(this).parent().siblings('.apartment_id').val();
	$('#apartment_plain_modal').css({
	    'display': 'block'
	});
	$('.info .apartment_id').val(apartmentId);
	$.post('/apartment/'+apartmentId+'/show_plains', {'_token':token}, function(data){
	    $.each(data, function(elem, val){
		$('#plain_collection').append('<div class="info">'+
		    '<img data-order="'+val['order']+'" data-id="'+val['id']+'"'+
		    'src="/storage/'+val['src']+'" class="apartment_plain_img" > '+
		    '<input type="text" class="description" value="'+val['description']+'">'+
		    '<input type="text" class="meta_description" value="'+val['meta_description']+'"> '+
		    '<input class="plain_id" type="hidden" value="'+val['id']+'" > '+
		    '<div class="info_wrap">'+
			'<button id="delete_plain">Удалить</button>'+
			'<button class="update_plain">Обновить</button>'+
		    '</div>'+
		    '</div>');
	    });
	});
	return false;
    });
    
    
	// по клику сохранть планировку отправляем ее на сервер
    $('#add_apartment_plain').click(function(){
	var apartment_id = $(this).siblings('.apartment_id').val();
	var plain = $('#input_apartment_plain').prop('files')[0];
	var plainData = new FormData();
	plainData.append('plain', plain);
	plainData.append('_token', token);
	
	$.ajax({
	    url: '/apartment/'+apartment_id+'/upload_plain',
	    dataType: 'json',
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: plainData,
	    type: 'POST',
	    success: function(data){
		$('#input_apartment_plain').val('');
		
		$('#plain_collection').append('<div class="info">'+
		    '<img data-order="'+data.order+'" data-id="'+data.id+'"'+
		    'src="/storage/'+data.src+'" class="apartment_plain_img" > '+
		    '<input type="text" class="description" value="'+data.description+'">'+
		    '<input type="text" class="meta_description" value="'+data.meta_description+'"> '+
		    '<input class="plain_id" type="hidden" value="'+data.id+'" > '+
		    '<div class="info_wrap">'+
			'<button id="delete_plain">Удалить</button>'+
			'<button class="update_plain">Обновить</button>'+
		    '</div>'+
		    '</div>');
	    }    
	});
	return false;
    });
    
	// закрывает модальное окно с планировками
    $('.close_plain_modal').click(function(){
	$('#apartment_plain_modal').css({
	    'display': 'none'
	});
	$('#plain_collection').html('');
    });
	// обновляет описание и мета описание планировки квартиры по id планировки
    $('body').on('click', '.update_plain', function(){
	var plain_id = $(this).parent().siblings('.plain_id').val();
	var description = $(this).parent().siblings('.description').val();
	var meta_description = $(this).parent().siblings('.meta_description').val();
	var data = {'description':description, 'meta_description':meta_description, '_token':token};
	$.post('/apartment/'+plain_id+'/update_plain', data, function(data){
	    if(data >0){
		$('#modal_msg').show().html('<p>Успешно обновлено</p>');
		$('#modal_msg').delay(2000).hide(500);
	    }
	});
    });
	// удаляет квартиру по id квартиры
    $('.delete_apartment').click(function(){
	console.log('del');
	var apartment_id = $(this).parent().siblings('.apartment_id').val();
	var tr = $(this).parent().parent('tr');
	$.post('/apartment/'+apartment_id+'/delete_apartment', {'_token':token},
	    function(data){
		if(data>0){
		   tr.remove();
		   $('#message').show().html('Успешно');
		   $('#message').delay(1000).hide(500).html();
		}else{
		    $('#message').show().html('Ошибка удаления квартиры');
		    $('#message').delay(2000).hide(500).html('');
		}
	    }
	);
	return false;
    });
	
    $('body').on('click', '#delete_plain', function(){
	var info = $(this).parent().parent();
	var plain_id = $(this).parent().siblings('.plain_id').val();
	$.post('/apartment/'+plain_id+'/delete_plain', {'_token':token}, function(data){
	    if(data>0){
		info.remove();
	    }else{
		info.prepend('Ошибка удаления');
	    }
	});
    });
	// изменяет порядок расположения планировок квартиры
    $( "#plain_collection" ).sortable({ tolerance:"intersect" }).disableSelection();
    
    $('#change_plain_order').click(function(){
	var data_array = [];
	    // проходим по каждому элементу и собираем данные
	$('#plain_collection .info img').each(function(e){  
	    $(this).data('order', e+1);
	    var id = $(this).data('id');
	    var order = $(this).data('order');
	    data_array[e] = {'id':id, 'order':order};
	});
	var apartment_id = $('.apartment_id').val();
	var data = {'_token':token, 'images_arr':data_array};
	$.ajax({
	    url: '/apartment/'+apartment_id+'/change_plain_order',
	    type: 'POST',
	    data: data,
	    success: function(data){
		$('#modal_msg').show().html('<p>'+data+'</p>');
		$('#modal_msg').delay(2000).hide(500);
		
	    }
	});
	return false;
    });
    
});
</script>
@endsection