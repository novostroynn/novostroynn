@extends('admin.layouts.panel')
@section('content')
<div class="button-panel">
    <a href="/manager/house/{{$house_id}}/edit">Назад</a>
    <hr>
    
</div>
<div class="workspace">
<h2>Страница редактирования хода строительства дома</h2>

<div class="info_wrap">
    <div class="info">
	<p>Ход строительства</p>
	<div class="progress_of_construction" >   
	    @for($i=0; $i < count($house_progress); $i++)
		@if( !isset( $house_progress[$i-1]->year ) )
		    <!--- первый месяц првого года -->
		    <h2> {{ $house_progress[$i]->year }} </h2>
		    <br><b> {{ $house_progress[$i]->name }}</b><br>
			<!-- Открываем блок info_wrap после названия первого месяца  первого года  -->
		    <div class="info_wrap sortable"> 
			<div class="info">
			    <img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
				data-id="{{$house_progress[$i]->img_id}}"
				src="/storage/{{ $house_progress[$i]->src }}" 
				alt="{{$house_progress[$i]->meta_description}}">
			    <label for="description">Описание</label>
			    <input type="text" name="description" class="description" 
				   value="{{$house_progress[$i]->description}}">
			    <label for="meta_description">Мета описание</label>
			    <input type="text" name="meta_desription" class="meta_description"
				   value="{{$house_progress[$i]->meta_description}}">
			    <div class="info_wrap">
				<button class="delete_progress">Удалить</button>
				<button class="change_description">Обновить</button>
			    </div>
			    <button class="update_order">Обновить порядок картинок</button>
			</div>
		@elseif( $house_progress[$i]->year == $house_progress[$i-1]->year )
		    @if( $house_progress[$i]->month != $house_progress[$i-1]->month )
			<!-- выводим блок нового месяца -->
		    </div> <!-- Закрываем блок info_wrap когда в этом месяце больше нет картинок -->
	    
		    <br><b> {{ $house_progress[$i]->name }} </b><br>
			<!-- открываем блок info_wrap после названия следующего месяца этого года-->
		    <div class="info_wrap sortable">
		    <div class="info">
			<img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
			     data-id="{{$house_progress[$i]->img_id}}"
			     src="/storage/{{ $house_progress[$i]->src }}" 
			     alt="{{$house_progress[$i]->meta_description}}">
			<label for="description">Описание</label>
			<input type="text" name="description" class="description"
			       value="{{$house_progress[$i]->description}}">
			<label for="meta_description">Мета описание</label>
			<input type="text" name="meta_desription" class="meta_description"
			       value="{{$house_progress[$i]->meta_description}}">
			<div class="info_wrap">
			    <button class="delete_progress">Удалить</button>
			    <button class="change_description">Обновить</button>    
			</div>
			<button class="update_order">Обновить порядок картинок</button>
		    </div>
		    @else
			<!-- следующая картинка месца -->
			<div class="info">
			    <img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
				 data-id="{{$house_progress[$i]->img_id}}"
				 src="/storage/{{ $house_progress[$i]->src }}" 
				 alt="{{$house_progress[$i]->meta_description}}">
			    <label for="description">Описание</label>
			    <input type="text" name="description" class="description"
				   value="{{$house_progress[$i]->description}}">
			    <label for="meta_description">Мета описание</label>
			    <input type="text" name="meta_desription" class="meta_description"
				   value="{{$house_progress[$i]->meta_description}}">
			    <div class="info_wrap">
				<button class="delete_progress">Удалить</button>
				<button class="change_description">Обновить</button>
			    </div>
			</div>
		    @endif
		@else
		    <!-- закрываем блок info_wrap когда больше нет картинок в последнем месяце года -->
		</div> 
		<hr>
		<h2> {{ $house_progress[$i]->year }}</h2>
		<br><b> {{ $house_progress[$i]->name }}</b><br>
		<div class="info_wrap sortable"> <!-- открываем блок info_wrap в первом месяце следующего года -->
		    <div class="info">
			<img class="progress_img" data-order="{{$house_progress[$i]->order }}" 
			     data-id="{{$house_progress[$i]->img_id}}"
			     src="/storage/{{ $house_progress[$i]->src }}" 
			     alt="{{$house_progress[$i]->meta_description}}">
			<label for="description">Описание</label>
			<input type="text" name="description" class="description"
			       value="{{$house_progress[$i]->description}}">
			<label for="meta_description">Мета описание</label>
			<input type="text" name="meta_desription" class="meta_description"
			       value="{{$house_progress[$i]->meta_description}}">
			<div class="info_wrap">
			    <button class="delete_progress">Удалить</button>
			    <button class="change_description">Обновить</button>
			</div>
			<button class="update_order">Обновить порядок картинок</button>
		    </div>
		@endif
	    @endfor
	    
	</div> <!--  info-wrap --> 
    </div> <!-- info-wrap -->
    <p>Добавить ход строительства</p>
	    <form action="/manager/{{$house_id}}/add_progress_image" enctype="multipart/form-data" method="POST">
		{{csrf_field()}}
		<input type="hidden" id="house_id" name="house_id" value="{{$house_id}}">
		<input type="hidden" name="order" value="0">
		<div class="info">
		    <input type="file" name="progress" id="progress">
		    <label for="description">Описание</label>
		    <input type="text" name="description" class="description">
		    <label for="meta_description">мета описание</label>
		    <input type="text" name="meta_description" class="meta_description">
		    <select name="year" id="">
			<option value="2015">2015</option>
			<option value="2016">2016</option>
			<option value="2017">2017</option>
			<option value="2018">2018</option>
			<option value="2019">2019</option>
			<option value="2020">2020</option>
			<option value="2021">2021</option>
			<option value="2022">2022</option>
			<option value="2023">2023</option>
		    </select>
		    <select name="month" id="">
			<option value="">Выберите месяц</option>
			@foreach($months as $month)
			    <option value="{{$month->id}}">{{$month->name}}</option>
			@endforeach
			<option value="6">Июнь</option>
		    </select>
		    <input type="submit" value="Отправить">
		</div>
	    </form>
    </div> <!-- info -->
    </div> <!-- info_wrap -->
</div> <!-- workspace -->

<script type="text/javascript" src="{{ URL::asset('js/jquery-ui-1.12.1.custom/jquery-ui.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
	$( ".sortable" ).sortable({ tolerance:"intersect" }).disableSelection();
	var house_id = $('#house_id').val();
	var token = '<?php echo csrf_token() ?>';
	    // по клику на кнопку обновить порядок проходим по каждой картинке
	    // в блоке этого месяца и изменяем параметр data-order начиная 
	    // с 1, потом сохраняем обновляенный порядок картинок в БД
	$('.update_order').click(function(){
	    var data_array = [];
	    $(this).parent().parent('.info_wrap').children('.info').children('img').each(function(e){
		$(this).data('order', e+1);
		var id = $(this).data('id');
		var order = $(this).data('order');
		data_array[e] = {'id':id, 'order':order};
	    });
	    
	    var token = '<?php echo csrf_token() ?>';
	    var data = {'_token':token, 'progress_arr':data_array};

	    $.ajax({
		url: '/manager/'+house_id+'/change_progress_order',
		type: 'POST',
		data: data,
		success: function(data){
		    $('#message').show(500).html(data);
		    $('#message').delay(2000).hide(500);
		}
	    });
	    return false;
	});
	
	    // по клику на кнопку обновить получаем данные из полей описание 
	    // и мета-описание текущей картинки, ее id и сохраняем в БД
	$('.change_description').click(function(){
	    var description = $(this).parent().siblings('.description').val();
	    var meta_description = $(this).parent().siblings('.meta_description').val();
	    var image_id = $(this).parent().siblings('img').data('id');
	    var data = {'description':description, 'meta_description':meta_description, '_token':token};
	    
	    $.ajax({
		url: '/manager/'+house_id+'/progress/'+image_id,
		type: 'POST',
		data:data,
		success: function(data){
		    $('#message').show(500).html(data);
		    $('#message').delay(2000).hide(500);		    
		}
	    });
	});
	
	    // по клику на кнопку удалить - удаляем картинку хода строитесльва id картинки
	    // которую нужно удалить передаем в url потом редиректим
	    // на страницу хода строительства этого дома
	$('.delete_progress').click(function(){
	    var image_id = $(this).parent().siblings('img').data('id');
	    
	    $.post(
		'/manager/'+house_id+'/delete_progress_image/'+image_id,
		{ _token:token },
		function(data){
		    if(data > 0){
			location.reload();
		    }else{
			$('#message').show().html(data);
			$('#message').delay(2000).hide();
		    }
		}
	    );
	});
    });
</script>
    
@endsection

