@extends('admin.layouts.panel')
@section('content')
<div class="button-panel">
    <a href="/manager/housing_estates/{{$housing_estate_id}}/edit">Назад</a>
    <hr>
</div>
<div class="workspace">
    <h2>Страница добавления дома</h2>
    <form action="/manager/house" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="housing_estate_id" value="{{$housing_estate_id}}">
    <div class="info_wrap">
	    <label for="published">Опубликован</label>
	    <div class="info_wrap">	
		<input type="radio" name="published" value="0" checked>нет
		<input type="radio" name="published" value="1"> да
	    </div>
    </div>
    <div class="info_wrap">
	<div class="info">
	    <label for="name" >Назвaние дома</label>
	    <input type="text" name="name" required>
	    <label for="count_floors">Количество этажей</label>
	    <input type="text" name="count_floors">
	    <label for="count_entranses">Количество подъездов</label>
	    <input type="text" name="count_entranses">
	    <label for="count_apartments">Количество квартир</label>
	    <input type="text" name="count_apartments"> 
	    <label for="ceiling_height">Высота потолков</label>
	    <input type="text" name="ceiling_height">    
	</div> <!-- info -->
	<div class="info">
	   <label for="start_construct">Начало строительства</label>
	    <select name="start_construct">
		<option value="">Начало строительства</option>
		@foreach($construct_dates as $construct_data)
		<option value="{{$construct_data->id}}">{{$construct_data->name}}</option>
		@endforeach
	    </select>
	    <label for="end_construct">Срок сдачи</label>
	    <select  name="end_construct">
		<option value="">Срок сдачи</option>
		@foreach($construct_dates as $construct_data)
		<option value="{{$construct_data->id}}">{{$construct_data->name}}</option>
		@endforeach
	    </select>
	    <label for="main_structure">Несущая конструкция</label>
	    <select name="main_structure">
		<option value="">Несущая конструкция</option>
		@foreach($main_structures as $main_structure)
		<option value="{{$main_structure->id}}">{{$main_structure->name}}</option>
		@endforeach
	    </select>
	    <label for="wall_material">Материал стен</label>
	    <select name="wall_material">
		<option value="">Материал стен</option>
		@foreach($wall_materials as $wall_material)
		<option value="{{$wall_material->id}}">{{$wall_material->name}}</option>
		@endforeach
	    </select>
	    <label for="apartment_finishing">Отделка квартир</label>
	    <select name="apartment_finishing">
		<option value="">Отделка квартир</option>
		@foreach($apartment_finishings as $apartment_finishing)
		<option value="{{$apartment_finishing->id}}">{{$apartment_finishing->name}}</option>
		@endforeach
	    </select> 
	</div> <!-- info -->
	<div class="info">
	    <label for="heating_type">Тип отопления</label>
	    <select name="heating_type" id="">
		<option value="">Тип отопления</option>
		@foreach($heating_types as $heating_type)
		<option value="{{$heating_type->id}}">{{$heating_type->name}}</option>
		@endforeach
	    </select>
	    <label for="cooker_type">Тип кухонной плиты</label>
	    <select name="cooker_type" id="">
		<option value="">Тип кухонной плиты</option>
		@foreach($cooker_types as $cooker_type)
		<option value="{{$cooker_type->id}}">{{$cooker_type->name}}</option>
		@endforeach
	    </select>
	    <label for="window_installation">Материал остекления</label>
	    <select name="window_installation" id="">
		<option value="">Тип остекления</option>
		@foreach($window_installations as $window_installation)
		<option value="{{$window_installation->id}}">{{$window_installation->name}}</option>
		@endforeach
	    </select>
	    <label for="build">Сдан</label>
	    <div class="info_wrap">	
		<input type="radio" name="build" value="0" checked>не сдан
		<input type="radio" name="build" value="1"> сдан
	    </div>
	</div>
    </div> <!-- info_wrap -->
    <input type="submit" id="save_house" class="workspace_save" value="Сохранить дом">
    </form>
    
</div> <!-- workspace -->

@endsection

