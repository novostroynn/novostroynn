<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Тип отопления
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($heating_types)>0)
                @foreach($heating_types as $heating_type)
                    <span class="parameter" data-id="{{ $heating_type['id'] }}">{{ $heating_type['name'] }}</span>
                @endforeach
            @else
                Нет списка видов отопления
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить тип отделки квартир</p>
            <form  method="POST" id="heating_type_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="heating_type_name">
                <input type="button" value="Сохранить" id="save_heating_type" >
		<input type="button" value="Обновить" class="update_parameter" data-table="heating_types">
            </form>
            <span id="heating_type_error" class='parameters_error'></span>
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_heating_type').click(function(){
            var heating_type_name = $('#heating_type_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(heating_type_name === ''){
                $('#heating_type_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':heating_type_name, '_token':token};
            $.ajax({
                url: '/parameter/heating_type_add',
                type: "POST",
                data: data,
                success: function(data){
                    $('#heating_type_error').html(data);
                    $('#heating_type_name').val('');
                }
            });
        });
        $('#heating_type_name').focus(function(){
            $('#heating_type_error').text('');
        });
    });
</script>




