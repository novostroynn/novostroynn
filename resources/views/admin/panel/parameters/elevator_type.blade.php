<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Тип лифтов
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($elevator_types)>0)
                @foreach($elevator_types as $elevator_type)
                    <span class="parameter" data-id="{{ $elevator_type['id'] }}">{{ $elevator_type['name'] }}</span>
                @endforeach
            @else
                Нет списка лифтов
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить тип лифта</p>
            <form  method="POST" id="elevator_type_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="elevator_type_name">
                <input type="button" value="Сохранить" id="save_elevator_type" >
		<input type="button" value="Обновить" class="update_parameter" data-table="elevator_types">
            </form>
            <span id="elevator_type_error" class='parameters_error'></span>
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_elevator_type').click(function(){
            var elevator_type_name = $('#elevator_type_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(elevator_type_name === ''){
                $('#elevator_type_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':elevator_type_name, '_token':token};
            $.ajax({
                url: '/parameter/elevator_type_add',
                type: "POST",
                data: data,
                success: function(data){
                    $('#elevator_type_error').html(data);
                    $('#elevator_type_name').val('');
                }
            });
        });
        $('#elevator_type_name').focus(function(){
            $('#elevator_type_error').text('');
        });
    });
</script>


