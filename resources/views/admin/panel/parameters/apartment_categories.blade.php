<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Категории квартир
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($apartment_categories)>0)
                @foreach($apartment_categories as $apartment_category)
                    <span class="parameter" data-id="{{ $apartment_category->id }}">{{ $apartment_category->name }}</span>
                @endforeach
            @else
		<p>Нет списка категорий квартир</p>
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить категорию квартир</p>
            <form  method="POST" id="apartment_category_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="apartment_category_name">
                <input type="button" value="Сохранить" id="save_apartment_category" >
		<input type="button" value="Обновить" class="update_parameter" data-table="apartment_categories">
            </form>
            <span id="apartment_category_error" class='parameters_error'></span>
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_apartment_category').click(function(){
            var apartment_category_name = $('#apartment_category_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(apartment_category_name === ''){
                $('#apartment_category_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':apartment_category_name, '_token':token};
            $.ajax({
                url: '/parameter/apartment_category_add',
                type: "POST",
                data: data,
                success: function(data){
		    if(data > 0){
			$('#message').html('Сохранено успешно').show();
			$('#message').delay(2000).hide();
			$('#apartment_category_name').val('');
			location.reload();
		    }else{
			$('#message').html('Ошибка сохранения').show();
			$('#message').delay(2000).hide();
		    }
                }
            });
        });
        $('#apartment_category_name').focus(function(){
            $('#message').text('').hide();
        });
    });
</script>

