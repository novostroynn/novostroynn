<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Mатериал остекления 
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($window_installations)>0)
                @foreach($window_installations as $window_installation)
                    <span class="parameter" data-id="{{ $window_installation['id'] }}">{{ $window_installation['name'] }}</span>
                @endforeach
            @else
                Нет списка материалов остекления
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить материал остекления</p>
            <form  method="POST" id="window_installation_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="window_installation_name">
                <input type="button" value="Сохранить" id="save_window_installation" >
		<input type="button" value="Обновить" class="update_parameter" data-table="window_installations">
            </form>
            <span id="window_installation_error" class='parameters_error'></span>
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_window_installation').click(function(){
            var window_installation_name = $('#window_installation_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(window_installation_name === ''){
                $('#window_installation_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':window_installation_name, '_token':token};
            $.ajax({
                url: '/parameter/window_installation_add',
                type: "POST",
                data: data,
                success: function(data){
                    $('#window_installation_error').html(data);
                    $('#window_installation_name').val('');
                }
            });
        });
        $('#window_installation_name').focus(function(){
            $('#window_installation_error').text('');
        });
    });
</script>