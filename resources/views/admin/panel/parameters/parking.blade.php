<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Парковки 
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($parking_types)>0)
                @foreach($parking_types as $parking_type)
                    <span class="parameter" data-id="{{ $parking_type['id'] }}">{{ $parking_type['name'] }}</span>
                @endforeach
            @else
                <span class="parameter">Нет паpаметров</span>
            @endif
            
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить тип парковки</p>
            <form  method="POST" id="parking_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="parking_name">
                <input type="button" value="Сохранить" id="save_parking_type" >
		<input type="button" value="Обновить" class="update_parameter" data-table="parking_types">
            </form>
            <span id="parking_error" class='parameters_error'></span>
        </div>
        
    </div>
</div> <!-- parameter_category_wrap -->
<script type="text/javascript">
    $('document').ready(function(){
        $('#save_parking_type').click(function(){
            var parking_name = $('#parking_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(parking_name == ''){
                $('#parking_error').text('Заполните поле');
                return false;
            }
            var data = {'name': parking_name, '_token':token};
            $.ajax({
                url: '/parameter/parking_type_add',
                type: 'POST',
                data: data,
                success: function(data){
                    $('#parking_error').text('Сохранено');
                    $('#parking_name').val('');
                }
            }); 
        });
        
        $('#parking_name').focus(function(){
            $('#parking_error').text('');
        });
    });
</script>

