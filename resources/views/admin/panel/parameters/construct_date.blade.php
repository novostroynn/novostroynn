<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Список дат
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($construct_dates)>0)
                @foreach($construct_dates as $construct_date)
                    <span class="parameter" data-id="{{ $construct_date['id'] }}">{{ $construct_date['name'] }}</span>
                @endforeach
            @else
                Нет списка дат
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить дату</p>
            <form  method="POST" id="heating_type_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="construct_date_name">
                <input type="button" value="Сохранить" id="save_construct_date" >
		<input type="button" value="Обновить" class="update_parameter" data-table="construct_dates">
            </form>
            <span id="construct_date_error" class='parameters_error'></span>
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_construct_date').click(function(){
            var construct_date_name = $('#construct_date_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(construct_date_name === ''){
                $('#construct_date_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':construct_date_name, '_token':token};
            $.ajax({
                url: '/parameter/construct_date_add',
                type: "POST",
                data: data,
                success: function(data){
                    $('#construct_date_error').html(data);
                    $('#construct_date_name').val('');
                }
            });
        });
        $('#construct_date_name').focus(function(){
            $('#construct_date_error').text('');
        });
    });
</script>






