<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Метериал стен 
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($wall_materials)>0)
                @foreach($wall_materials as $wall_material)
                    <span class="parameter" data-id="{{ $wall_material['id'] }}">{{ $wall_material['name'] }}</span>
                @endforeach
            @else
                Не найдено метерилов стен
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить материал стен</p>
            <form  method="POST" id="wall_material_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="wall_material_name">
                <input type="button" value="Сохранить" id="save_wall_material" >
		<input type="button" value="Обновить" class="update_parameter" 
		    data-table="wall_materials" >
            </form>
            <span id="wall_material_error" class='parameters_error'></span>
        </div>
        
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_wall_material').click(function(){
            var wall_material_name = $('#wall_material_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(wall_material_name === ''){
                $('#wall_material_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':wall_material_name, '_token':token};
            $.ajax({
                url: '/parameter/wall_material_add',
                type: "POST",
                data: data,
                success: function(data){
                    $('#wall_material_error').text(data);
                    $('#wall_material_name').val('');
                }
            });
        });
        $('#wall_material_name').focus(function(){
            $('#wall_material_error').text('');
        });
    });
</script>


