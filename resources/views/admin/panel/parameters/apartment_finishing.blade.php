<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Тип отделки квартир
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($apartment_finishings)>0)
                @foreach($apartment_finishings as $apartment_finishing)
                    <span class="parameter" data-id="{{ $apartment_finishing['id'] }}">{{ $apartment_finishing['name'] }}</span>
                @endforeach
            @else
                Нет списка отделок квартир квартир
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить тип отделки квартир</p>
            <form  method="POST" id="apartment_finishing_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="apartment_finishing_name">
                <input type="button" value="Сохранить" id="save_apartment_finishing" >
		<input type="button" value="Обновить" class="update_parameter" data-table="apartment_finishings">
            </form>
            <span id="apartment_finishing_error" class='parameters_error'></span>
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_apartment_finishing').click(function(){
            var apartment_finishing_name = $('#apartment_finishing_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(apartment_finishing_name === ''){
                $('#apartment_finishing_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':apartment_finishing_name, '_token':token};
            $.ajax({
                url: '/parameter/apartment_finishing_add',
                type: "POST",
                data: data,
                success: function(data){
                    $('#apartment_finishing_error').html(data);
                    $('#apartment_finishing_name').val('');
                }
            });
        });
        $('#apartment_finishing_name').focus(function(){
            $('#apartment_finishing_error').text('');
        });
    });
</script>





