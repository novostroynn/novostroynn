<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Метериал несущей конструкции 
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($main_structures)>0)
                @foreach($main_structures as $main_structure)
                    <span class="parameter" data-id="{{ $main_structure['id'] }}">{{ $main_structure['name'] }}</span>
                @endforeach
            @else
                Нет списка несущих конструкций
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить материал несущей конструкции</p>
            <form  method="POST" id="main_structure_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="main_structure_name">
                <input type="button" value="Сохранить" id="save_main_structure" >
		<input type="button" value="Обновить" class="update_parameter" data-table="main_structures">
            </form>
            <span id="main_structure_error" class='parameters_error'></span>
        </div>
        
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_main_structure').click(function(){
            var main_structure_name = $('#main_structure_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(main_structure_name === ''){
                $('#main_structure_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':main_structure_name, '_token':token};
            $.ajax({
                url: '/parameter/main_structure_add',
                type: "POST",
                data: data,
                success: function(){
                    $('#main_structure_error').text('Сохранено');
                    $('#main_structure_name').val('');
                }
            });
        });
        $('#main_structure_name').focus(function(){
            $('#main_structure_error').text('');
        });
    });
</script>
