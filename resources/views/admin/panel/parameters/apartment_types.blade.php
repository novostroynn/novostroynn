<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Типы квартир
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap">
            @if(count($apartment_types)>0)
                @foreach($apartment_types as $apartment_type)
                    <span class="parameter" data-id="{{ $apartment_type->id }}">{{ $apartment_type->name }}</span>
                @endforeach
            @else
		<p>Нет списка типов квартир</p>
            @endif
        </div>
        <hr>
        <div class="add_parameter">
            <p>Добавить тип квартир</p>
            <form  method="POST" id="apartment_type_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="apartment_type_name">
		<select id="apartment_category">
		    <option value="">Выберите категорию</option>
		    @foreach($apartment_categories as $apartment_category)
		    <option value="{{$apartment_category->id}}">{{$apartment_category->name}}</option>
		    @endforeach
		</select>
                <input type="button" value="Сохранить" id="save_apartment_type">
		<input type="button" value="Обновить" data-table="apartment_types" class="update_parameter">
            </form>
            <span id="apartment_type_error" class='parameters_error'></span>
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_apartment_type').click(function(){
            var apartment_type_name = $('#apartment_type_name').val();
	    var apartment_category_id = $('#apartment_category').val();

            var token = '<?php echo csrf_token() ?>';
            if(apartment_type_name === ''){
                $('#apartment_type_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':apartment_type_name, 'category_id':apartment_category_id, '_token':token};
            $.ajax({
                url: '/parameter/apartment_type_add',
                type: "POST",
                data: data,
                success: function(data){
		    if(data > 0){
			$('#message').html('Сохранено успешно').show();
			$('#message').delay(2000).hide();
			$('#apartment_type_name').val('');
			$('#apartment_category').val('');
			location.reload();
		    }else{
			$('#message').html('Ошибка сохранения').show();
			$('#message').delay(2000).hide();
		    }
                }
            });
        });
        $('#apartment_type_name').focus(function(){
            $('#message').text('').hide();
        });
    });
</script>

