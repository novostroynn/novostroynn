<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Регионы 
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        @if(count($location_areas)>0)
	    <p class="location_region">Нижний Новгород</p>
            <div class="parameter_wrap" data-category="location_region">
                @if ($location_areas)
                    @foreach($location_areas as $areas)
                        @if($areas['location_region_id'] == 1)
                        <span class="parameter" data-id="{{$areas['id']}}">{{ $areas['name'] }}</span>
                        @endif
                    @endforeach
                @else
                    Районов не найдено
                @endif 
            </div>
	    <hr>

	    <p class="location_region">Нижегородская область</p>
            <div class="parameter_wrap" >
                @if(1)
                    @foreach($location_areas as $areas)
                        @if($areas['location_region_id'] == 2)
                             <span class="parameter" data-id="{{$areas['id']}}">{{ $areas['name'] }}</span>
                        @endif
                    @endforeach
                @else
                    Районов не найдено
                @endif 
            </div>
        @else 
            Районов не найдено
        @endif
        <hr>
        <div class="add_parameter">
            <p>Добавить район</p>
            <form  method="POST" id="area_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="area_name">
                <select name="location_regions" id="location_region">
                    <option value="0">Выберите район</option>
                    <option value="1">Нижний Новгород</option>
                    <option value="2">Нижегородская область</option>
                </select>
                <input type="button" value="Сохранить" id="save_location_area" >
		<input type="button" value="Обновить" class="update_parameter" data-table="location_areas">
            </form>
            <span id="location_error" class='parameters_error'></span>
        </div>

    </div> <!-- parameter_list -->
</div> <!-- parameter_category_wrap -->
<script type="text/javascript">
        // сохраняет район города или области
    $('#save_location_area').click(function(){
        var name = $('#area_name').val();
        var location_region = $('#location_region').val();
        if(location_region == 0){
            $('#location_error').text('Укажите регион');
            return false;
        }
        if(name == ''){
            $('#location_error').text('Укажите название района');
            return false;
        }

        var token = '<?php echo csrf_token() ?>';
        var data = {'name':name, 'location_region':location_region, '_token':token};
        $.ajax({
            url: '/parameter/location_area_add',
            type: 'POST',
            data: data,
    //                headers: {
    //                    'X-CSRF-Token':$('meta[name="csrf-token"]').attr('content')
    //                },
            success: function(){
                $('#location_error').text('Сохранено');
                $('#area_name').val('');
                $('#location_region').val(0);
            }
        });
        console.log( name +'---'+ location_region );
    });
    $('#area_name').focus(function(){
            $('#location_error').text('');
        });
        $('#location_region').focus(function(){
            $('#location_error').text('');
    });
</script>