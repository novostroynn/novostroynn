<div class="parameter_category_wrap">
    <p class='parameter_list_title'>
        Тип кухонных плит 
        <span class="show_block">+</span>
        <span class="hide_block">-</span>
    </p>
    <div class="parameter_list">
        <div class="parameter_wrap" >
            @if(count($cooker_types)>0)
                @foreach($cooker_types as $cooker_type)
                    <span class="parameter" data-id="{{ $cooker_type['id'] }}">{{ $cooker_type['name'] }}</span>
                @endforeach
            @else
                Нет списка кухонных плит
            @endif
        </div>
        <hr>
        
        <div class="add_parameter">
            <p>Добавить тип кухонной плиты</p>
            <form  method="POST" id="cooker_type_form">
                {{ csrf_field() }}
                <input type="text" name="name" id="cooker_type_name">
                <input type="button" value="Сохранить" id="save_cooker_type" >
		<input type="button" value="Обновить" class="update_parameter" data-table="cooker_types">
            </form>
            <span id="cooker_type_error" class='parameters_error'></span>
	    
        </div> 
    </div>
</div> <!-- parameter_category_wrap -->
<script type='text/javascript'>
    $('document').ready(function(){
        $('#save_cooker_type').click(function(){
            var cooker_type_name = $('#cooker_type_name').val();
            var token = '<?php echo csrf_token() ?>';
            if(cooker_type_name === ''){
                $('#cooker_type_error').text('Поле не заполнено');
                return false;
            }
            var data = {'name':cooker_type_name, '_token':token};
            $.ajax({
                url: '/parameter/cooker_type_add',
                type: "POST",
                data: data,
                success: function(data){
                    $('#cooker_type_error').html(data);
                    $('#cooker_type_name').val('');
                }
            });
        });
        $('#cooker_type_name').focus(function(){
            $('#cooker_type_error').text('');
        });
    });
</script>