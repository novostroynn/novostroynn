@extends('layouts.index')


@section('title', 'Заголовок стартовой страницы')

@section('content')
    <form class="search_form" action="" method="POST">
	<div class="search_wrap">
	    <div class="qwe">
		<div class="search_form_field_wrap">
		    <div class="field_group location_group">
			<div class="location_wrap">
			@foreach($locations as $k=>$location)
			    @if($k == 0)
				<input id="location_radio_{{$k}}" type="radio" name="location" value="{{$location->id}}" checked>
			    @else
				<input id="location_radio_{{$k}}" type="radio" name="location" value="{{$location->id}}">
			    @endif
			    <label class="location_button" for="location_radio_{{$k}}">{{$location->name}}</label> 
			@endforeach
			</div>
			<input type="text" class="name_search" placeholder="Название жилого комплекса">
		    </div>  
		</div>
		<div class="search_form_field_wrap buttons_wrap">
		    <div class="field_group">
			<div class="button_wrap">
			   <div class="location_area search_form_button">Район</div> 
			    <ul class="location_area_list drop_down_list">
				@foreach($location_areas as $location_area)
				    <li>
					<input type="checkbox" id="location_area_{{$location_area->id}}" value="{{$location_area->id}}">
					<label for="location_area_{{$location_area->id}}">{{$location_area->name}}</label>
				    </li>
				@endforeach
			    </ul>  
			</div>
			<div class="button_wrap">
			<div class="apartment_category search_form_button">Количество комнат</div>
			<ul class="apartment_category_list drop_down_list">
			    @foreach($apartment_categories as $apartment_category)
				<li>
				    <input type="checkbox" id="apartment_category_{{$apartment_category->id}}" value="{{$apartment_category->id}}">
				    <label for="apartment_category_{{$apartment_category->id}}">{{$apartment_category->name}}</label>
				</li>
			    @endforeach
			</ul> 
			</div>
		    </div>
		    <div class="field_group">
			<div class="button_wrap">
			   <div class="construct_date search_form_button">Срок сдачи</div> 
			    <ul class="construct_date_list drop_down_list">
				@foreach($construct_dates as $construct_date)
				    <li>
					<input type="checkbox" id="construct_date_{{$construct_date->id}}" value="{{$construct_date->id}}">
					<label for="construct_date_{{$construct_date->id}}">{{$construct_date->name}}</label>
				    </li>
				@endforeach
			    </ul>  
			</div>
			<div class="button_wrap">
			<div class="apartment_finishing search_form_button">Отделка</div>
			<ul class="apartment_finishing_list drop_down_list">
			    @foreach($apartment_finishings as $apartment_finishing)
				<li>
				    <input type="checkbox" id="apartment_finishing_{{$apartment_finishing->id}}" value="{{$apartment_finishing->id}}">
				    <label for="apartment_finishing_{{$apartment_finishing->id}}">{{$apartment_finishing->name}}</label>
				</li>
			    @endforeach
			</ul> 
			</div>
		    </div>
		    <div class="input_wrap">
			<div class="field_group price_group">
			    <input type="text" placeholder="Цена от">
			    <input type="text" placeholder="Цена до">
			</div>
			<div class="field_group area_group">
			    <input type="text" placeholder="Площадь от">
			    <input type="text" placeholder="Площадь до">
			</div>
		    </div>
		</div>
		</div>
	    <div class="search_btn_wrap  ">
		<button class="search_btn">Поиск</button>
	    </div>

	</div>
    </form>
<div class="banner_top">
    <p>Блок с </p>
    <p>рекламой</p>
</div>
<div class="content">
    <div class="map_wrap">
	<h1>БАЗА НОВОСТРОЕК</h1>
	<div id="map">
	    
	</div>
    </div>
    <div class="news_wrap">
	<h3>НОВОСТИ</h3>
    </div>
</div>
<div class="banner_bottom">
    <p>Блок </p>
    <p>с </p>
    <p>рекламой</p>
</div>
<script type="text/javascript">
    var coords= JSON.parse('<?php echo $coordinates; ?>');

</script>
<script type="text/javascript" src="{{ URL::asset('js/page_ymaps.js') }}">
<script type="text/javascript">
    $(document).ready(function(){
	    // скрываем и показываем модальные окна в поисковой форме
	$('body').click(function(e){
	    var button = $('.search_form_button');
	    var div = $('.drop_down_list');
	    
	    if( 
	    	(!button.is(e.target) && button.has(e.target).length === 0) 
		    &&
		(!div.is(e.target) && div.has(e.target).length === 0)  
	    ){
		$('.drop_down_list').removeClass('show');
	    }
	});
	
	$('.location_area').click(function(){
	    if($(this).siblings('.drop_down_list').hasClass('show')){
		$('.drop_down_list').removeClass('show');
	    }else{
		$('.drop_down_list').removeClass('show');
		$('.location_area_list').toggleClass('show');
	    }
	
	});
	$('.apartment_category').click(function(){
	    if($(this).siblings('.drop_down_list').hasClass('show')){
		$('.drop_down_list').removeClass('show');
	    }else{
		$('.drop_down_list').removeClass('show');
		$('.apartment_category_list').toggleClass('show');
	    }
	});
	$('.construct_date').click(function(){
	    if($(this).siblings('.drop_down_list').hasClass('show')){
		$('.drop_down_list').removeClass('show');
	    }else{
		$('.drop_down_list').removeClass('show');
		$('.construct_date_list').toggleClass('show');
	    }
	});
	$('.apartment_finishing').click(function(){
	    if($(this).siblings('.drop_down_list').hasClass('show')){
		$('.drop_down_list').removeClass('show');
	    }else{
		$('.drop_down_list').removeClass('show');
		$('.apartment_finishing_list').toggleClass('show');
	    }
	});
    });
</script>
@endsection
