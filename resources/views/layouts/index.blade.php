<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css" />
	<script type="text/javascript" src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"
  type="text/javascript">
  </script>
        <title>@yield('title')</title>
    </head>
    <body>
        <div class="wrap">
            @include('layouts.header')
            <div class="page-wrap">
                @yield('content')
            </div>
            @include('layouts.footer')
        </div>
    </body>
</html>