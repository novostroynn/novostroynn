
<header>
    
    <ul class="header-menu">
        <a href="">
            <li>НОВОСТРОЙКИ</li>
        </a>
        <a href="">
            <li>КОММЕРЧЕСКАЯ НЕДВИЖИМОСТЬ</li>
        </a>
        <a href="">
            <li>КОТТЕДЖНЫЕ ПОСЕЛКИ</li>
        </a>
        <a href="">
            <li>ЗАСТРОЙЩИКИ</li>
        </a>
        <a href="">
            <li>ГОРОДСКИЕ ОБЪЕКТЫ</li>
        </a>
        <a href="">
            <li>БИБЛИОТЕКА</li>
        </a>
	<a href="" class="advertising ">
	    <li>Реклама на сайте</li>
	</a>
    </ul> 
    <div class="top_background">
	<button class="cmn-toggle-switch cmn-toggle-switch__rot">
	    <span>toggle menu</span>
	</button>
	<div class="logo"></div>
<!--        <img src="{{ URL::asset('images/backgrounds/header_background.jpg') }}" alt="Поиск квартир в новостройках Нижнего Новгорода" class='top_background_img'>-->
    </div>
</header>

<script type="text/javascript">

(function() {
    "use strict";
    var toggle = document.querySelector(".cmn-toggle-switch");
    var top_menu = document.querySelector("header .header-menu");
    
    toggleHandler(toggle);
    showMenu(top_menu, toggle);
    
    function toggleHandler(toggle) {
	toggle.addEventListener( "click", function(e) {
	    e.preventDefault();
	    (this.classList.contains("active") === true) ? this.classList.remove("active") : this.classList.add("active");

	    });
	}
    function showMenu(top_menu, toggle) {
	toggle.addEventListener( "click", function(e) {
	    e.preventDefault();
	    console.log(top_menu.classList.contains('show'));
	    (top_menu.classList.contains("show") === true) ? top_menu.classList.remove("show") : top_menu.classList.add("show");

	    });
	}

})();
</script>
