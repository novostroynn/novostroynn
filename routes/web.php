<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
     * пока непонятно это это такое
     */
/*
 * класс создает набор роутов для аутентификации, смены пароля, выхода и т.п.
 */

//Route::get('/auth', function(){
//    return view('admin.authorization.login');
//});
//Auth::routes(); 

    /*
     * Главная страница
     */
Route::get('/', 'Pages\HousingEstatesController@index');

   
    /*
     * маршруты для аутентификации пользователя
     */
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

    /*
     * Контроллер для админа, авторизация, выход, проверка прав
     */
Route::get('/manager', 'Manager\ManagerController@index');


    /*
     * Контроллер для работы с параметрами для новостроек
     * создание, просмотр
     */
Route::get('/manager/parameters', 'Manager\ParametersController@parameters_page');
Route::post('/parameter/location_area_add', 'Manager\ParametersController@add_area');	
Route::post('/parameter/parking_type_add', 'Manager\ParametersController@add_parking_type');
Route::post('/parameter/main_structure_add', 'Manager\ParametersController@add_main_structure');
Route::post('/parameter/wall_material_add', 'Manager\ParametersController@add_wall_material');
Route::post('/parameter/window_installation_add', 'Manager\ParametersController@add_window_installation');
Route::post('/parameter/cooker_type_add', 'Manager\ParametersController@add_cooker_type');
Route::post('/parameter/elevator_type_add', 'Manager\ParametersController@add_elevator_type');
Route::post('/parameter/apartment_finishing_add', 'Manager\ParametersController@add_apartment_finishing');
Route::post('/parameter/heating_type_add', 'Manager\ParametersController@add_heating_type');
Route::post('/parameter/construct_date_add', 'Manager\ParametersController@add_construct_date');
Route::post('/parameter/location/{region_id}', 'Manager\ParametersController@show_area');
Route::post('/parameter/apartment_category_add', 'Manager\ParametersController@add_apartment_category');
Route::post('/parameter/apartment_type_add', 'Manager\ParametersController@add_apartment_type');

Route::post('/parameter/{parameter_id}/{table_name}', 'Manager\ParametersController@update_parameter');


/* Контроллеры для работы с застройщиками и продавцами
 * 
 */
Route::resource('manager/builder', 'Manager\BuilderController');

Route::resource('manager/seller', 'Manager\SellerController');


    /*
     * контроллер для работы с жилыми комплексами, создание, удаление, просмотр
     * редактирование
     */
Route::post('manager/housing_estates', 'Manager\HousingEstatesController@store');
Route::post('/manager/housing_estates/{id}/meta_update', 'Manager\HousingEstatesController@meta_update');
Route::post('/manager/housing_estates/{id}/logo_update', 'Manager\HousingEstatesController@logo_update');
Route::post('/manager/housing_estates/{id}/add_gallery_img', 'Manager\ImagesController@add_housing_estate_gallery_img');
Route::post('/manager/{house_estate_id}/delete_gallery_image/{image_id}', 'Manager\ImagesController@del_housing_estate_gallery_image');
Route::post('/manager/{housing_estate_id}/gallery_image/{image_id}', 'Manager\ImagesController@update_gallery_image_description');
Route::post('/manager/{housingestate_id}/change_gallery_images_order', 'Manager\ImagesController@change_gallery_images_order');

Route::resource('manager/housing_estates', 'Manager\HousingEstatesController');

/*
     * контроллер для работы с домами с жилом комлексе
     */
	    // планировки
Route::get('/manager/housing_estate/{housing_estate_id}/create_house', 'Manager\HouseController@create');
Route::get('/manager/house/{house_id}/house_progress', 'Manager\ImagesController@show_house_progress');
Route::post('/manager/{house_id}/save_plain', 'Manager\ImagesController@save_plain');
Route::post('/manager/{house_id}/change_plain_order', 'Manager\ImagesController@change_plain_order');
Route::post('/manager/{house_id}/edit_plain/{image_id}', 'Manager\ImagesController@update_plain_description');
Route::post('/manager/{house_id}/delete_plain_image/{image_id}', 'Manager\ImagesController@delete_plain_image');
	    // ход строительства
Route::post('/manager/{house_id}/add_progress_image', 'Manager\ImagesController@add_progress_image');
Route::post('/manager/{house_id}/change_progress_order', 'Manager\ImagesController@change_progress_order');
Route::post('/manager/{house_id}/progress/{image_id}', 'Manager\ImagesController@update_progress_description');
Route::post('/manager/{house_id}/delete_progress_image/{image_id}', 'Manager\ImagesController@delete_progress_images');
    
Route::resource('manager/house', 'Manager\HouseController');

    /*
     * контроллер для работы с квартирами
     */
Route::post('/manager/{house_id}/add_apartments', 'Manager\ApartmentsController@add_apartment');
Route::post('/manager/house/{house_id}/update_apartments', 'Manager\ApartmentsController@update_apartments');
Route::post('/apartment/{apartment_id}/delete_apartment', 'Manager\ApartmentsController@delete_apartment');
Route::post('/apartment/{apartment_id}/upload_plain', 'Manager\ApartmentsController@upload_apartment_plain');
Route::post('/apartment/{apartmentId}/show_plains', 'Manager\ApartmentsController@show_apartment_plains');
Route::post('/apartment/{plain_id}/delete_plain', 'Manager\ApartmentsController@delete_apartment_plain');
Route::post('/apartment/{plain_id}/update_plain', 'Manager\ApartmentsController@update_plain');
Route::post('/apartment/{apartment_id}/change_plain_order', 'Manager\ApartmentsController@change_plain_order');

    

    