<?php

use Illuminate\Database\Seeder;

class ConstructDatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('construct_dates')->insert(
	    ['name'=> '1 квартал 2018']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '2 квартал 2018']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '3 квартал 2018']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '4 квартал 2018']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '1 квартал 2019']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '2 квартал 2019']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '3 квартал 2019']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '4 квартал 2019']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '1 квартал 2020']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '2 квартал 2020']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '3 квартал 2020']
	);
	DB::table('construct_dates')->insert(
	    ['name'=> '4 квартал 2020']
	);
    }
}
