<?php

use Illuminate\Database\Seeder;

class MonthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('month')->insert([
	    'name' => 'Январь'
	]);
	DB::table('month')->insert([
	    'name' => 'Февраль'
	]);
	DB::table('month')->insert([
	    'name' => 'Март'
	]);
	DB::table('month')->insert([
	    'name' => 'Апрель'
	]);
	DB::table('month')->insert([
	    'name' => 'Май'
	]);
	DB::table('month')->insert([
	    'name' => 'Июнь'
	]);
	DB::table('month')->insert([
	    'name' => 'Июль'
	]);
	DB::table('month')->insert([
	    'name' => 'Август'
	]);
	DB::table('month')->insert([
	    'name' => 'Сентябрь'
	]);
	DB::table('month')->insert([
	    'name' => 'Октябрь'
	]);
	DB::table('month')->insert([
	    'name' => 'Ноябрь'
	]);
	DB::table('month')->insert([
	    'name' => 'Декабрь'
	]);
    }
}