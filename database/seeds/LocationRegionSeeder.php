<?php

use Illuminate\Database\Seeder;

class LocationRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_regions')->insert([
            'name' => 'Нижний Новгород'
        ]);
	DB::table('location_regions')->insert([
            'name' => 'Нижегородская область'
        ]);
	
    }
}
