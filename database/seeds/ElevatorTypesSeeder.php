<?php

use Illuminate\Database\Seeder;

class ElevatorTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elevator_types')->insert([
	    'name'=>'Пассажирский'
	]);
    }
}
