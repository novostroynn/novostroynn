<?php

use Illuminate\Database\Seeder;

class CookerTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cooker_types')->insert([
	    'name'=>'газовая'
	]);
	DB::table('cooker_types')->insert([
	    'name'=>'электрическая'
	]);
    }
}
