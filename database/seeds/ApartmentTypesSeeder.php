<?php

use Illuminate\Database\Seeder;

class ApartmentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('apartment_types')->insert([
            'name' => 'Студия',
	    'apartment_category' => 1
        ]);
    }
}
