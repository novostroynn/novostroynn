<?php

use Illuminate\Database\Seeder;

class ApartmentFinishingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('apartment_finishings')->insert([
	    'name'=>'Без отделки'
	]);
	DB::table('apartment_finishings')->insert([
	    'name'=>'С отделкой'
	]);
    }
}
