<?php

use Illuminate\Database\Seeder;

class HeatingTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('heating_types')->insert([
	    'name'=>'Центральное'
	]);
    }
}
