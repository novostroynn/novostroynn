<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
	    MonthSeeder::class,
	    AgentSeeder::class,
	    ApartmentFinishingsSeeder::class,
	    CookerTypesSeeder::class,
	    ElevatorTypesSeeder::class,
	    HeatingTypesSeeder::class,
	    UsersTableSeeder::class,
	    LocationRegionSeeder::class,
	    ConstructDatesSeeder::class,
	]);
    }
}
