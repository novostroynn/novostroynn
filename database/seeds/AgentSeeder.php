<?php

use Illuminate\Database\Seeder;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agent_types')->insert([
            'name' => 'Застройщики'
        ]);
	DB::table('agent_types')->insert([
            'name' => 'Продавцы'
        ]);
    }
}
