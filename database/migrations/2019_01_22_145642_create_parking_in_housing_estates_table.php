<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingInHousingEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_in_housing_estates', function (Blueprint $table) {
            $table->unsignedSmallInteger('parking_id');
            $table->unsignedInteger('estate_id');
            $table->unsignedSmallInteger('count')->nullable();
            
            $table->foreign('parking_id')->references('id')->on('parking_types')->onDelete('cascade');
            $table->foreign('estate_id')->references('id')->on('housing_estate')->onDelete('cascade');
            
	    $table->unique(['parking_id', 'estate_id']);
	    
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_in_housing_estates');
    }
}
