<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousingEstatesGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('housing_estates_gallery', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('description', 255)->nullable();
	    $table->string('meta_description', 255)->nullable();
	    $table->unsignedInteger('housing_estates_id');
	    $table->string('src', 255);
	    $table->unsignedTinyInteger('order')->default(0);
	    
	    $table->foreign('housing_estates_id')->references('id')->on('housing_estate')->onDelete('cascade');
	    
	    $table->engine = 'InnoDB';
	    $table->charset = 'utf8';
	    $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('housing_estates_gallery');
    }
}
