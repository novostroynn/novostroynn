<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentPlainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_plains', function (Blueprint $table) {
            $table->increments('id')->unsigned();
	    $table->unsignedInteger('apartment_id');
	    $table->string('src', 255);
	    $table->string('description', 255)->nullable();
	    $table->string('meta_description', 255)->nullable();
	    $table->unsignedTinyInteger('order')->default(0);
	    
	    $table->foreign('apartment_id')->references('id')->on('apartments')->onDelete('cascade');
	    
	    $table->engine = 'InnoDB';
	    $table->charset = 'utf8';
	    $table->collation = 'utf8_general_ci';
            
		    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartment_plains');
    }
}
