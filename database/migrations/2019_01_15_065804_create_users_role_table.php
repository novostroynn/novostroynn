<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRoleTable extends Migration
{
    /**
     * Таблица соотношения ролей пользователей. строки уникальны,
     * поля - внешние ключи пользователей и ролей
     * удаляются каскадом
     * @return void
     */
    public function up()
    {
        Schema::create('users_role', function (Blueprint $table) {
            /*
             * Создает таблицу ролей пользователей
             */   
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('role_id');

            $table->unique('user_id', 'role_id');
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_role');
    }
}
