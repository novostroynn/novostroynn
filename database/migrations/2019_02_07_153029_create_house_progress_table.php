<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_progress', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('description', 255)->nullable();
	    $table->string('meta_description', 255)->nullable();
	    $table->string('src', 255);
	    $table->unsignedInteger('house_id');
	    $table->unsignedSmallInteger('year');
	    $table->unsignedTinyInteger('month');
	    $table->unsignedTinyInteger('order')->default(0);
	    
	    $table->foreign('house_id')->references('id')->on('houses')->onDelete('cascade');
	    $table->foreign('month')->references('id')->on('month')->onDelete('cascade');
	    
	    $table->engine = 'InnoDB';
	    $table->charset = 'utf8';
	    $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_progress');
    }
}
