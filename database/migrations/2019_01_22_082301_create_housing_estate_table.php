<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousingEstateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('housing_estate', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->string('alias', 255);
            $table->string('meta_title', 100)->nullable();
            $table->string('meta_description', 200)->nullable();
	    $table->string('logo_src', 255)->nullable();
	    $table->string('logo_description', 255)->nullable();
            $table->unsignedSmallInteger('location_area')->nullable();
            $table->string('address', 255)->nullable();
            $table->float('latitude', 18, 15)->nullable();
            $table->float('longitude', 18, 15)->nullable();
            $table->unsignedTinyInteger('published')->default(0);
            $table->smallInteger('start_construct')->unsigned()->nullable();
            $table->smallInteger('end_construct')->unsigned()->nullable();
            $table->smallInteger('wall_material')->unsigned()->nullable();
            $table->smallInteger('count_houses')->unsigned()->nullable();
            $table->smallInteger('count_apartments')->unsigned()->nullable();
            $table->smallInteger('main_structure')->unsigned()->nullable();
            $table->smallInteger('heating_type')->unsigned()->nullable();
	    $table->text('description')->nullable();
	    $table->unsignedInteger('creater')->nullable();
	    $table->unsignedInteger('updater')->nullable();
	    $table->unsignedTinyInteger('blocked')->default(0);
            $table->timestamps();	    
            
            $table->foreign('location_area')->references('id')->on('location_areas')->onDelete('cascade');
            $table->foreign('start_construct')->references('id')->on('construct_dates')->onDelete('cascade');
            $table->foreign('end_construct')->references('id')->on('construct_dates')->onDelete('cascade');
            $table->foreign('wall_material')->references('id')->on('wall_materials')->onDelete('cascade');
            $table->foreign('main_structure')->references('id')->on('main_structures')->onDelete('cascade');
            $table->foreign('heating_type')->references('id')->on('heating_types')->onDelete('cascade');
	    $table->foreign('creater')->references('id')->on('users')->onDelete('cascade');
	    $table->foreign('updater')->references('id')->on('users')->onDelete('cascade');
            
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('housing_estate');
    }
}
