<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->smallIncrements('id')->unsigned();
            $table->string('name', 255);
            $table->string('address', 255)->nullable();
            $table->string('logo_src', 255)->nullable();
	    $table->string('logo_description', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('phone', 255)->nullable();
	    $table->string('email', 255)->nullable();
	    $table->string('site', 255)->nullable();
            $table->tinyInteger('type_agent')->unsigned();
	    $table->tinyInteger('published')->unsigned()->default(0);
	    $table->float('latitude', 18,15);
	    $table->float('longitude', 18,15);

            $table->foreign('type_agent')->references('id')
                    ->on('agent_types')->onDelete('cascade');

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
