<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('apartment_type');
	    $table->unsignedInteger('house_id');
	    $table->string('floor', 45)->nullable();
	    $table->float('total_area', 5, 2)->nullable();
	    $table->float('living_area', 5, 2)->nullable();
	    $table->float('kitchen_area', 5, 2)->nullable();
	    $table->unsignedSmallInteger('finishing_type')->nullable();
	    $table->unsignedInteger('price')->nullable();
	    
	    $table->foreign('apartment_type')->references('id')->on('apartment_types')->onDelete('cascade');
	    $table->foreign('house_id')->references('id')->on('houses')->onDelete('cascade');
	    $table->foreign('finishing_type')->references('id')->on('apartment_finishings')->onDelete('cascade');
	    
	    $table->engine = 'InnoDB';
	    $table->charset = 'utf8';
	    $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
