<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationAreasTable extends Migration
{
    /**
     * Run the migrations.
     * таблица локаций для ЖК, содержит название района и привязку района к региону
     * @return void
     */
    public function up()
    {
        Schema::create('location_areas', function (Blueprint $table) {
            $table->smallIncrements('id')->unsigned();
            $table->string('name', 50)->unique();
            $table->smallInteger('location_region_id')->unsigned();
            
            
            $table->foreign('location_region_id')->references('id')
                    ->on('location_regions')->onDelete('cascade');
            
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_areas');
    }
}
