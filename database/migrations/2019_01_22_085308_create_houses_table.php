<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100);
            $table->unsignedInteger('housing_estate_id')->nullable();
            $table->string('count_floors', 50)->nullable();
            $table->unsignedTinyInteger('count_entranses')->nullable();
            $table->unsignedSmallInteger('count_apartments')->nullable();
            $table->unsignedSmallInteger('start_construct')->nullable();
            $table->unsignedSmallInteger('end_construct')->nullable();
            $table->unsignedSmallInteger('main_structure')->nullable();
            $table->unsignedSmallInteger('wall_material')->nullable();
            $table->unsignedSmallInteger('apartment_finishing')->nullable();
            $table->float('ceiling_height', 3, 2)->nullable();
            $table->unsignedSmallInteger('heating_type')->nullable();
            $table->unsignedSmallInteger('cooker_type')->nullable();
            $table->unsignedSmallInteger('window_installation')->nullable();
	    $table->unsignedInteger('creater')->nullable();
	    $table->unsignedInteger('updater')->nullable();
	    $table->unsignedTinyInteger('build')->default(0);
            $table->unsignedTinyInteger('published')->default(0);
	    $table->unsignedTinyInteger('blocked')->default(0);
            $table->timestamps();
	    
            $table->foreign('housing_estate_id')->references('id')->on('housing_estate')->onDelete('cascade');
            $table->foreign('start_construct')->references('id')->on('construct_dates')->onDelete('cascade');
            $table->foreign('end_construct')->references('id')->on('construct_dates')->onDelete('cascade');
            $table->foreign('main_structure')->references('id')->on('main_structures')->onDelete('cascade');
            $table->foreign('wall_material')->references('id')->on('wall_materials')->onDelete('cascade');
            $table->foreign('apartment_finishing')->references('id')->on('apartment_finishings')->onDelete('cascade');
            $table->foreign('heating_type')->references('id')->on('heating_types')->onDelete('cascade');
            $table->foreign('cooker_type')->references('id')->on('cooker_types')->onDelete('cascade');
            $table->foreign('window_installation')->references('id')->on('window_installations')->onDelete('cascade');
	    $table->foreign('creater')->references('id')->on('users')->onDelete('cascade');
	    $table->foreign('updater')->references('id')->on('users')->onDelete('cascade');
            
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
