<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsToHousingEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents_to_housing_estates', function (Blueprint $table) {
            $table->unsignedInteger('housing_estate_id');
	    $table->unsignedSmallInteger('agent_id');
	    
	    $table->foreign('housing_estate_id')->references('id')->on('housing_estate')->onDelete('cascade');
	    $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
	    
	    $table->unique(['housing_estate_id', 'agent_id']);
	    
	    $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents_to_housing_estates');
    }
}
