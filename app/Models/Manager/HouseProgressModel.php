<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class HouseProgressModel extends Model
{
    protected $table = 'house_progress';
    
    protected $fillable = [
	'description',
	'meta_description',
	'house_id',
	'src',
	'year',
	'month',
	'order'
    ];
    public $timestamps = false;
}
