<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class AgentsToHousingEstates extends Model
{
    protected $table = 'agents_to_housing_estates';
    protected $guarded = [];
    public $timestamps = false;
}
