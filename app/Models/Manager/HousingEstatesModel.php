<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class HousingEstatesModel extends Model
{
    protected $fillable = [
        'name',
        'alias',
        'meta_title',
        'meta_description',
	'logo_src',
	'logo_description',
        'location_area',
        'address',
        'latitude',
        'longitude',
        'description',
        'published',
        'start_construct',	
        'end_construct',	
        'wall_material', 	
        'count_houses',
        'count_apartments',
        'main_structure',
        'heating_type',
	'creater',
	'updater'
    ];
    //protected $guarded = [];
    
    protected $table = 'housing_estate';
    
}
