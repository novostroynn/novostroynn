<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class HousePlainModel extends Model
{
    protected $table = 'house_plain_images';
    
    protected $fillable = [
	'description',
	'meta_description',
	'src',
	'order',
	'house_id'
    ];
    public $timestamps = false;
}
