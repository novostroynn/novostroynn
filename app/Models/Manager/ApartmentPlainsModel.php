<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class ApartmentPlainsModel extends Model
{
    protected $table = 'apartment_plains';
    protected $fillable = [
	'apartment_id',
	'src',
	'description',
	'meta_description',
	'order'
    ];
    public $timestamps = false;
}
