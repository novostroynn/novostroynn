<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = [
	'name',
	'description',
	'address',
	'logo_src',
	'logo_description',
	'phone',
	'email',
	'site',
	'type_agent',
	'latitude',
	'longitude',
	'published'
    ];
    
    protected $table='agents';
    
    public $timestamps = false;
}
