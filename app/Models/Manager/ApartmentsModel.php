<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

use Yadakhov\InsertOnDuplicateKey;

class ApartmentsModel extends Model
{
    use InsertOnDuplicateKey;
    
    protected $table='apartments';
    
    protected $fillable=[
	'apartment_type',
	'house_id',
	'floor',
	'total_area',
	'living_area',
	'kitchen_area',
	'finishing_type',
	'price'
    ];
    public $timestamps = false;
	    
}
