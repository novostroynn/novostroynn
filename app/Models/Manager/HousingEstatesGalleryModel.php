<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class HousingEstatesGalleryModel extends Model
{
    protected $table = 'housing_estates_gallery';
    protected $fillable = [
	'description',
	'meta_description',
	'housing_estates_id',
	'src',
	'order'
    ];
    public $timestamps = false;
}
