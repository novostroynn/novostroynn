<?php

namespace App\Models\Manager;

use Illuminate\Database\Eloquent\Model;

class HousesModel extends Model
{
    protected $table = 'houses';
    
    protected $fillable = [
	"name",
	"housing_estate_id",
	"count_floors",
	"count_entranses",
	"count_apartments",
	"start_construct",
	"end_construct",
	"build",
	"main_structure",
	"wall_material",
	"apartment_finishing",
	"ceiling_height",
	"heating_type",
	"cooker_type",
	"window_installation",
	"published"
    ];
}
