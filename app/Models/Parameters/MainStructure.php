<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class MainStructure extends Model
{
    protected $table = 'main_structures';
    
    public $timestamps = false;
}
