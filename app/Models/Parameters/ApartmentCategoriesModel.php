<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class ApartmentCategoriesModel extends Model
{
    protected $table = 'apartment_categories';
    protected $fillable = [
	'name'
    ];
    public $timestamps = false;
}
