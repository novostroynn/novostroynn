<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class LocationRegions extends Model
{
    protected $table = 'location_regions';
    
    public $timestamps = false;
}
