<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class ConstructDate extends Model
{
    protected $table = 'construct_dates';
    
    public $timestamps = false;
}
