<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class CookerType extends Model
{
    protected $table = 'cooker_types';
    
    public $timestamps = false;
}
