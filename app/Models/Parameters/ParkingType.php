<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

class ParkingType extends Model
{
        // таблица с которой работает модель
    protected $table = 'parking_types';
    
    public $timestamps = false;
}
