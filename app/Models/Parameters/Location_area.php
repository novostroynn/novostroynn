<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;


use Illuminate\Http\Request;

class Location_area extends Model
{
    protected $fillable = [
        'name', 'location_region_id'
    ];
    protected $table = 'location_areas';
    
    public $timestamps = false;
    
    public function add_area(Request $request){
        $area = new Location_area;
        $area->name = $request->name;
        $area->location_region_id = $request->location_region;
        
    }
}
