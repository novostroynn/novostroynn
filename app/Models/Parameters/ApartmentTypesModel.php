<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class ApartmentTypesModel extends Model
{
    protected $table = 'apartment_types';
    protected $fillable = [
	'name',
	'category_id'
    ];
    public $timestamps = false;
}
