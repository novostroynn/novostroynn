<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class ElevatorType extends Model
{
    protected $table = 'elevator_types';
    
    public $timestamps = false;
}
