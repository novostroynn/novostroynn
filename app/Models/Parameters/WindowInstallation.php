<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class WindowInstallation extends Model
{
    protected $table = 'window_installations';
    
    public $timestamps = false;
}
