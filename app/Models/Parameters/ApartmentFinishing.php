<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class ApartmentFinishing extends Model
{
    protected $table = 'apartment_finishings';
    
    public $timestamps = false;
}
