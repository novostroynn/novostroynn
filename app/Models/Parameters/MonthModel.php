<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class MonthModel extends Model
{
    protected $table="month";
    
    public $timestamps = false;
}
