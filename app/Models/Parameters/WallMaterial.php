<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class WallMaterial extends Model
{
    protected $table = 'wall_materials';
    
    public $timestamps = false;
}
