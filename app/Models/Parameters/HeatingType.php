<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;

class HeatingType extends Model
{
    protected $table = 'heating_types';
    
    public $timestamps = false;
}
