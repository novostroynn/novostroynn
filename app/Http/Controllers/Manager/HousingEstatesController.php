<?php

namespace App\Http\Controllers\Manager;

use App\Models\Manager\HousingEstatesModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Parameters\LocationRegions;
use App\Models\Parameters\ConstructDate;
use App\Models\Parameters\MainStructure;
use App\Models\Parameters\WallMaterial;
use App\Models\Parameters\ParkingType;
use App\Models\Parameters\HeatingType;
use App\Models\Manager\Builder;
use App\Models\Manager\Seller;
use App\Models\Manager\AgentsToHousingEstates;
use App\Models\Manager\HousesModel;
use App\Models\Manager\HousingEstatesGalleryModel;

class HousingEstatesController extends Controller
{
    public function __construct(){
            // на все методы этого класса будет срабатывать проверка авторизации
        $this->middleware('auth');
    }
    
    /*
	     * Метод создает название загружаемого изображения, принимает
	     * id объекта, название объекта, название изображения
	     */
    public function getImageName($id_object, $alias, $image){
	
	$image_name = $id_object.'_'.$alias;
	$point = strrpos($image, '.');
	$mime = substr($image, $point);
	$image_name = str_replace(' ', '_', $image_name);
	$image_name .= $mime;
	
	return $image_name;
		
    }
    
    
    /**
     * Страница вывода списка жилых комплексов
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function index()
    {
        $housing_estates = HousingEstatesModel::all()->toArray();
        return view('admin.panel.housing_estates.index', compact('housing_estates'));
    }

    /**
     * Страница создания нового жилого комплекса
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location_regions = LocationRegions::all()->toArray();
        $construct_dates = ConstructDate::all()->toArray();
        $main_structures = MainStructure::all()->toArray();
        $wall_materials = WallMaterial::all()->toArray();
        $parking_types = ParkingType::all()->toArray();
        $heating_types = HeatingType::all()->toArray();
	$builders = Builder::where('type_agent', '=', 1)->get();
	$sellers = Seller::where('type_agent', '=', 2)->get();
	$user_id = Auth::user()->id;
        return view('admin.panel.housing_estates.create', compact('location_regions', 'construct_dates', 'main_structures', 'wall_materials', 'parking_types', 'heating_types', 'builders', 'sellers', 'user_id'));
    }

    /**
     * Метод сохраняет данные жилого комплекса из формы и 
     * переадресовывает на страницу редактирования
     * этого жилого комплекса
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
	// перевод русских букв в латиницу
    private function translit($string){
	$converter = array(
	    'а' => 'a',   'б' => 'b',   'в' => 'v',
	    'г' => 'g',   'д' => 'd',   'е' => 'e',
	    'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
	    'и' => 'i',   'й' => 'j',   'к' => 'k',
	    'л' => 'l',   'м' => 'm',   'н' => 'n',
	    'о' => 'o',   'п' => 'p',   'р' => 'r',
	    'с' => 's',   'т' => 't',   'у' => 'u',
	    'ф' => 'f',   'х' => 'h',   'ц' => 'c',
	    'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
	    'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
	    'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

	    'А' => 'A',   'Б' => 'B',   'В' => 'V',
	    'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
	    'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
	    'И' => 'I',   'Й' => 'Y',   'К' => 'K',
	    'Л' => 'L',   'М' => 'M',   'Н' => 'N',
	    'О' => 'O',   'П' => 'P',   'Р' => 'R',
	    'С' => 'S',   'Т' => 'T',   'У' => 'U',
	    'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
	    'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
	    'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
	    'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
	    ' ' => '-',
	);
	return strtr($string, $converter);
    }
    
    public function store(Request $request)
    {
        $housing_estate = new HousingEstatesModel($request->all());
	$housing_estate->alias = $this->translit($request->name);
	$housing_estate->save();
	    // если есть агенты, сохранить их в таблицу
	if(!empty($request->agents)){
	    for($i=0; $i<count($request->agents); $i++){
		if(is_null($request->agent[$i])){
		    continue;
		}
		DB::table('agent_to_housing_estates')->firstOrNew(
		    ['housing_estate_id'=>$housing_estate->id],
		    ['agent_id'=>$request->agents[$i]]
		);
	    }
	}
	    // если есть парковки, сохранить их в таблицу
	if(!empty($request->parking_type)){
	    for($i=0; $i<count($request->parking_type); $i++){
		if(is_null($request->parking_type[$i])){
		    continue;
		}
		DB::table('parking_in_housing_estates')->updateOrInsert(
		    ['parking_type_id'=>$request->parking_type[$i], 
		     'housing_estate_id'=>$request->id],
		    ['count'=>$request->parking_count[$i]]
		);
	    }
	}
	    // редирект на страницу редактирования ЖК
        return redirect('/manager/housing_estates/'.$housing_estate->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HousingEstatesModel  $housingEstatesModel
     * @return \Illuminate\Http\Response
     */
    public function show(HousingEstatesModel $housing_estate)
    {
        dump($housing_estate);
        exit();
        return view('');
    }

    /**
     * Метод показывает страницу редактирования жилого комплекса.
     * По id жилого комплекса получает массив парковок
     * для текущего жилого комплекса
     * @param  \App\Models\HousingEstatesModel  $housingEstatesModel
     * @return \Illuminate\Http\Response
     */
    public function edit(HousingEstatesModel $housingEstatesModel, $id)
    {  
        $housing_estate_arr = DB::table('housing_estate')
                ->select('housing_estate.*', 'location_areas.location_region_id')
                ->where('housing_estate.id', '=', $id)
                ->leftJoin('location_areas', 'housing_estate.location_area', '=', 'location_areas.id')
                ->get()
                ->toArray()
                ;
        $housing_estate = $housing_estate_arr[0];

	    $parking_arr = DB::table('parking_in_housing_estates')
		->select('parking_in_housing_estates.*', 'parking_types.name' )
		->where('parking_in_housing_estates.estate_id', '=',$housing_estate->id )
		->leftJoin('parking_types', 'parking_in_housing_estates.parking_id', '=', 'parking_types.id')
		->get();
	    
	    $agents_arr = DB::table('agents_to_housing_estates')
		    ->select('*')
		    ->where('agents_to_housing_estates.housing_estate_id', '=', $housing_estate->id)
		    ->leftJoin('agents', 'agents_to_housing_estates.agent_id', '=', 'agents.id')
		    ->get();
 
        $location_regions = LocationRegions::all()->toArray();
        $location_areas = \App\Models\Parameters\Location_area::all()->toArray();

        $construct_dates = ConstructDate::all()->toArray();
        $main_structures = MainStructure::all()->toArray();
        $wall_materials = WallMaterial::all()->toArray();
        $parking_types = ParkingType::all()->toArray();
        $heating_types = HeatingType::all()->toArray();
	
	$builders = Builder::where('type_agent', '=', 1)->get();
	$sellers = Seller::where('type_agent', '=', 2)->get();
	$houses = HousesModel::where('housing_estate_id', '=', $id)->get();
        
	$housing_estate_gallery_images = HousingEstatesGalleryModel::where('housing_estates_id', '=', $id)
		->orderBy('order')->get();
	$updater_id = Auth::user()->id;
        return view('admin.panel.housing_estates.edit', compact('housing_estate','location_regions', 'construct_dates', 'main_structures', 'wall_materials', 'parking_types', 'heating_types', 'location_areas', 'parking_arr', 'builders', 'sellers', 'agents_arr', 'updater_id', 'houses', 'housing_estate_gallery_images'));
    }
    
    /**
     * метод обновляет логотип жилого комплекса POST запросом
     * по id жилого комплекса
     */
    public function logo_update(Request $request, $id){

	$housing_estate = HousingEstatesModel::find($id);

	if($request->file('housing_estate_logo')){
	    $image_path = $this->getImageName($housing_estate->id, 'logo', $request->file('housing_estate_logo')->getClientOriginalName());
	    $housing_estate->logo_src = $request->file('housing_estate_logo')->storeAs('images/logo/housing_estates', $image_path );
	}
	$housing_estate->logo_description = $request->logo_description;
	
	if($housing_estate->update()){
	    return $housing_estate;
	}else{
	    return 0;
	}	
    }
    
    
    /**
     * метод обновляет мета информацию о жилом комплексе аякс запросом
     * по id жилого комплекса
     */
    public function meta_update(Request $request, $id){

	$housing_estate = HousingEstatesModel::find($id);
	$housing_estate->meta_title = $request->meta_title;
	$housing_estate->meta_description = $request->meta_description;
	if($housing_estate->update()){
	    return 'Изменения успешно сохранены';
	}else{
	    return 'Произошла ошибка сохранения';
	}
	
	
    }
    /**
     * метод обновляет в таблице данные жилого комплекса принимает запрос на обновление данных, 
     * обновляет жилой комплекс, обновляет праковки 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HousingEstatesModel  $housingEstatesModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $housing_estate = HousingEstatesModel::find($request->id);
	$housing_estate->alias = strtolower($this->translit($request->name));
	$housing_estate->update($request->all());
	
	    // если есть новые агенты, сохранить их в таблицу
	if(!empty($request->agents)){
	    for($i=0; $i<count($request->agents); $i++){
		if(is_null($request->agents[$i])){
			    continue;
			}
		$model = new AgentsToHousingEstates();
		$model::firstOrCreate(['housing_estate_id'=>$request->id, 'agent_id'=>$request->agents[$i] ]);
	    }    
	}
	    // если есть парковки сохранить 
	if(!empty($request->parking_type)){
	    for($i=0; $i<count($request->parking_type); $i++){
		if(is_null($request->parking_type[$i])){
		    break;
		}
		DB::table('parking_in_housing_estates')->updateOrInsert(
		    ['parking_id'=>$request->parking_type[$i], 
		     'estate_id'=>$request->id],
		    ['count'=>$request->parking_count[$i]]
		);
	    }
	}
	    // редирект на страницу редактирования ЖК
	return redirect('/manager/housing_estates/'.$request->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HousingEstatesModel  $housingEstatesModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(HousingEstatesModel $housingEstatesModel)
    {
        //
    }
}
