<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use App\Models\Manager\ApartmentsModel;
use App\Models\Manager\ApartmentPlainsModel;


class ApartmentsController extends Controller
{
    public function __construct()
    {
	$this->middleware('auth');
    }
	/*
	 * метод добавляет квартиры в таблицу квартир, получает масссив 
	 * квартир и id дома, редирект на страницу редактирования дома
	 */
    public function add_apartment(Request $request, $house_id)
    {
	
	$apartment_type = $request->apartment_type;
	$finishing_type = $request->apartment_finishing;
	$floor = $request->apartment_floor;
	$total_area = $request->apartment_total_area;
	$living_area = $request->apartment_living_area;
	$kitchen_area = $request->apartment_kitchen_area;
	$price = $request->apartment_price;
	
	$apartments = [];	
	for($i=0; $i<count($floor); $i++){
	    $apartments[$i]['apartment_type'] = $apartment_type[$i];
	    $apartments[$i]['finishing_type'] = $finishing_type[$i];
	    $apartments[$i]['house_id'] = $house_id;
	    $apartments[$i]['floor'] = $floor[$i];
	    $apartments[$i]['total_area'] = $total_area[$i];
	    $apartments[$i]['living_area'] = $living_area[$i];
	    $apartments[$i]['kitchen_area'] = $kitchen_area[$i];
	    $apartments[$i]['price'] = $price[$i];
	}
	
	DB::table('apartments')->insert($apartments);
	
	return redirect('/manager/house/'.$house_id.'/edit');

    }
	/*
	 * обновляет порядок отображения планировок квартиры
	 */
    public function change_plain_order(Request $request, $apartment_id)
    {
	$plains = ApartmentPlainsModel::where('apartment_id', '=', $apartment_id)->get();

	foreach($request->images_arr as $image){
	    foreach($plains as $plain){
		if($image['id'] == $plain->id){
		    $plain->order = $image['order'];
		    $plain->update();
		}
	    }
	}
	echo '<h3> Обновлено </h3>';
    }
    
	/*
	 * метод удаляет планировку квартиры по id планировки
	 */
    public function delete_apartment_plain($plain_id)
    {
	$plain = ApartmentPlainsModel::find($plain_id);
	Storage::delete($plain->src);
	if($plain->delete()){
	    return 1;
	}else{
	    return 0;
	}
    }
	/*
	 * метод получает изображения планировок для квартиры
	 */
    public function show_apartment_plains($apartment_id)
    {
	$apartment_plains = ApartmentPlainsModel::where('apartment_id', '=', $apartment_id)->orderBy('order')->get();
	return $apartment_plains;
	
    }
	/*
	 * метод обновляет все параметры всех квартир по их id
	 */
    public function update_apartments(Request $request, $house_id)
    {
	$apartments = [];
	$apartment_id = $request->apartment_id;
	$apartment_type = $request->apartment_type;
	$finishing_type = $request->apartment_finishing;
	$floor = $request->apartment_floor;
	$total_area = $request->apartment_total_area;
	$living_area = $request->apartment_living_area;
	$kitchen_area = $request->apartment_kitchen_area;
	$price = $request->apartment_price;
	
	for($i=0; $i<count($apartment_id); $i++){
	    $apartments[$i]['id'] = $apartment_id[$i]; 
	    $apartments[$i]['apartment_type'] = $apartment_type[$i];
	    $apartments[$i]['finishing_type'] = $finishing_type[$i];
	    $apartments[$i]['house_id'] = $house_id;
	    $apartments[$i]['floor'] = $floor[$i];
	    $apartments[$i]['total_area'] = $total_area[$i];
	    $apartments[$i]['living_area'] = $living_area[$i];
	    $apartments[$i]['kitchen_area'] = $kitchen_area[$i];
	    $apartments[$i]['price'] = $price[$i];
	}
	ApartmentsModel::insertOnDuplicateKey($apartments);
	
	return redirect('/manager/house/'.$house_id.'/edit');
    }
	
	/*
	 * метод удаляет квартиру по ее id и все планировки этой квартиры
	 */
    public function delete_apartment($apartment_id)
    {
	$apartment = ApartmentsModel::find($apartment_id);
	$plains = ApartmentPlainsModel::where('apartment_id', '=', $apartment_id)->get();
	foreach($plains as $plain){
	    Storage::delete($plain->src);
	}
	if($apartment->delete()){
	    return 1;
	}else{
	    return 0;
	}
    }
    
	/*
	 * метод добавляет фотографию планировки квартиры
	 * принимает изображение, параметры, id дома
	 */
    public function upload_apartment_plain(Request $request, $apartment_id)
    {	
	$apartment_plain = new ApartmentPlainsModel($request->all());
	$apartment_plain->apartment_id = $apartment_id;
	$image_name = $this->getImageName($apartment_id, 'apartment_plain', $request->file('plain')->getClientOriginalName());
	$apartment_plain->src =  $request->file('plain')->storeAs('images/apartments/plains', $image_name );
	
	$apartment_plain->save();
	return $apartment_plain;
    }
	/*
	 * обновляет описание и мета-описание планировок квартир
	 */
    public function update_plain(Request $request, $plain_id)
    {
	$plain = ApartmentPlainsModel::find($plain_id);
	$plain->description = $request->description;
	$plain->meta_description = $request->meta_description;
	if($plain->update()){
	    return 1;
	}else{
	    return 0;
	}
    }
	/*
	 * Метод создает название загружаемого изображения, принимает
	 * номер категории - type, название объекта, название изображения
	 */
    public function getImageName($id_object, $alias, $image)
    {
	$image_name = $id_object.'_'.$alias.'_'.md5(time());
	$point = strrpos($image, '.');
	$mime = substr($image, $point);
	$image_name = str_replace(' ', '_', $image_name);
	$image_name .= $mime;
	return $image_name;	
    }
    
    
}
