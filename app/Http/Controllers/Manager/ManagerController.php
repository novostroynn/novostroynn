<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{
    /*
     * методы класса отвечают за работу админ панели, вывод категорий
     * работа с пользователями и т.д.
     */
    public function __construct(){
            // на все методы этого класса будет срабатывать проверка авторизации
        $this->middleware('auth');
    }

    public function index() {
        /*
         * Главная страница админки
         */
        return view('admin.panel.index');
    }
    
}
