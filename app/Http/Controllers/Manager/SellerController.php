<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Manager\Seller;

class SellerController extends Controller
{
    /**
     * метод отображает список застройщиков на странице
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
	
	$this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellers = Seller::where('type_agent', '=', 2 )->get();
	return view('admin.panel.seller.index', compact('sellers'));
    }

    /**
     * Метод отображает страницу добавления нового застройщика
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.panel.seller.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seller = new Seller($request->all());
	
	
	
	if($seller->save()){
	    return redirect('manager/seller/'.$seller->id.'/edit');
	}
	else {
	    return 'Произошла ошибка сохранения';
	}

    }
	    /*
	     * Функция создает название загружаемого изображения, принимает
	     * id агента, название агента, название изображения
	     */
    public function getImageName($id_object, $alias, $image){
	
	$image_name = $id_object.'_'.$alias;
	$point = strrpos($image, '.');
	$mime = substr($image, $point);
	$image_name = str_replace(' ', '_', $image_name);
	$image_name .= $mime;
	
	return $image_name;
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = Seller::find($id);
	return view('admin.panel.seller.edit', compact('seller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seller = Seller::find($id);
	
	if($request->file('logo_src')){
	   $image_path = $this->getImageName($seller->id, 'logo', $request->file('logo_src')->getClientOriginalName());
	  // dd($image_path);
	   $seller->logo_src = $request->file('logo_src')->storeAs('images/logo/agents', $image_path );
	}else{
	   $seller->logo_src = $request->logo_src; 
	}
	
	$seller->name = $request->name;
	$seller->address = $request->address;
	$seller->logo_description = $request->logo_description;
	$seller->phone = $request->phone;
	$seller->email = $request->email;
	$seller->site = $request->site;
	$seller->published = $request->published;
	$seller->latitude = $request->latitude;
	$seller->longitude = $request->longitude;

	$seller->update();

	return redirect('/manager/seller/'.$seller->id.'/edit');
	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
