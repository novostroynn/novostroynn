<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Parameters\ConstructDate;
use App\Models\Parameters\MainStructure;
use App\Models\Parameters\WallMaterial;
use App\Models\Parameters\ApartmentFinishing;
use App\Models\Parameters\HeatingType;
use App\Models\Parameters\CookerType;
use App\Models\Parameters\WindowInstallation;
use App\Models\Parameters\ApartmentTypesModel;

use App\Models\Manager\HousePlainModel;
use App\Models\Manager\HousesModel;
use App\Models\Manager\ApartmentsModel;

class HouseController extends Controller
{
	/*
	 * на каждый метод класса будет срабатывать проверка авторизации
	 */
    public function __construct(){
	$this->middleware('auth');
    }
    
	    /*
	     * Метод создает название загружаемого изображения, принимает
	     * номер категории - type, название объекта, название изображения
	     */
    public function getImageName($id_object, $alias, $image){
	
	$image_name = $id_object.'_'.$alias.'_'.md5(time());
	$point = strrpos($image, '.');
	$mime = substr($image, $point);
	$image_name = str_replace(' ', '_', $image_name);
	$image_name .= $mime;
	return $image_name;	
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    

    /**
     * Метод отображает страницу добавления нового дома в жилой комплекс
     * принимает id жилого комплекса
     * @return \Illuminate\Http\Response
     */
    public function create($housing_estate_id){
	$construct_dates = ConstructDate::all();
	$main_structures = MainStructure::all();
	$wall_materials = WallMaterial::all();
	$apartment_finishings = ApartmentFinishing::all();
	$heating_types = HeatingType::all();
	$cooker_types = CookerType::all();
	$window_installations = WindowInstallation::all();
	
	return view('admin.panel.houses.create', compact('housing_estate_id','construct_dates', 'main_structures', 'wall_materials', 'apartment_finishings', 'heating_types', 'cooker_types', 'window_installations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $house = new HousesModel($request->all());
	$house->save();
	return redirect('/manager/house/'.$house->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($house_id)
    {
	$house = HousesModel::find($house_id);
	
	$construct_dates = ConstructDate::all();
        $main_structures = MainStructure::all();
        $wall_materials = WallMaterial::all();
	$apartment_finishings = ApartmentFinishing::all();
        $heating_types = HeatingType::all();
	$cooker_types = CookerType::all();
	$window_installations = WindowInstallation::all();
	$house_plains = HousePlainModel::where('house_id', '=', $house_id)->orderBy('order')->get();
	$apartment_types = ApartmentTypesModel::all();
	$apartments = ApartmentsModel::where('house_id', '=', $house_id)->get();
	

        return view('admin.panel.houses.edit', compact('house', 'construct_dates', 'main_structures', 'wall_materials', 'parking_types', 'heating_types', 'apartment_finishings', 'cooker_types', 'window_installations', 'house_plains', 'apartment_types', 'apartments'));
    }
    
    public function createTree($arr, $sub=0)
    {
	for($i=0; $i<count($arr); $i++){
	    if(!isset($arr[$i-1])){
		echo '<h2>'.$arr[$i]->year.'</h2>';
		echo '<b>'.$arr[$i]->month.'</b>';
		echo '<p>'.$arr[$i]->id.'</p>';
	    }elseif( $arr[$i]->year == $arr[$i-1]->year ){
		if( $arr[$i]->month != $arr[$i-1]->month ){
		    echo '<b>'.$arr[$i]->month.'</b>';
		    echo '<p>'.$arr[$i]->id.'</p>';
		}else{
		   echo '<p>'.$arr[$i]->id.'</p>'; 
		}
	    }else{
		echo '<h2>'.$arr[$i]->year.'</h2>';
		echo '<b>'.$arr[$i]->month.'</b>';
		echo '<p>'.$arr[$i]->id.'</p>';
	    }  
	}	    
    }
    
	
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $house = HousesModel::find($id);
	$house->update($request->all());
	return redirect('/manager/house/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
