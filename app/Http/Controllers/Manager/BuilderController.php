<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\Manager\Builder;

class BuilderController extends Controller
{
    /**
     * метод отображает список застройщиков на странице
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
	$this->middleware('auth');
    }
    
    public function index()
    {
        $builders = Builder::where('type_agent', '=', 1)->get();
	return view('admin.panel.builder.index', compact('builders'));
    }

    /**
     * Метод отображает страницу добавления нового затройщика
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.panel.builder.create');
    }

    /**
     * Метод принимает данные из формы добавления нового застройщика
     * сохраняет иредиректит на страницу этого застройщика
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $builder = new Builder($request->all());
	
	
	
	if($builder->save()){
	    return redirect('manager/builder/'.$builder->id.'/edit');
	}
	else {
	    return 'Произошла ошибка сохранения';
	}

    }
	    /*
	     * Метод создает название загружаемого изображения, принимает
	     * номер категории - type, название объекта, название изображения
	     */
    public function getImageName($id_object, $alias, $image){
	
	$image_name = $id_object.'_'.$alias;
	$point = strrpos($image, '.');
	$mime = substr($image, $point);
	$image_name = str_replace(' ', '_', $image_name);
	$image_name .= $mime;
	
	return $image_name;
		
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $builder = Builder::find($id);
	return view('admin.panel.builder.edit', compact('builder'));
    }

    /**
     * Метод обновляет данные о застройщике, сохраняет логотип, если его не было
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $builder = Builder::find($id);
	
	if($request->file('logo_src')){
	   $image_path = $this->getImageName($builder->id, 'logo', $request->file('logo_src')->getClientOriginalName());
	   $builder->logo_src = $request->file('logo_src')->storeAs('images/logo/agents', $image_path );
	}else{
	   $builder->logo_src = $request->logo_src; 
	}
	
	$builder->name = $request->name;
	$builder->address = $request->address;
	$builder->logo_description = $request->logo_description;
	$builder->phone = $request->phone;
	$builder->email = $request->email;
	$builder->site = $request->site;
	$builder->published = $request->published;

	$builder->latitude = $request->latitude;
	$builder->longitude = $request->longitude;

	$builder->update();

	return redirect('/manager/builder/'.$builder->id.'/edit');
	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
