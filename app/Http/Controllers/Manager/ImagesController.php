<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\Manager\HousePlainModel;
use App\Models\Manager\HouseProgressModel;
use App\Models\Parameters\MonthModel;
use App\Models\Manager\HousingEstatesGalleryModel;

class ImagesController extends Controller
{
    
	/*
	 * на каждый метод класса будет срабатывать проверка авторизации
	 */
    public function __construct(){
	$this->middleware('auth');
    }
	/*
	 * метод добавляет фотографию в галерею жилого комплекса
	 */
    public function add_housing_estate_gallery_img(Request $request, $housing_estate_id)
    {
	$housing_estate_gallery = new HousingEstatesGalleryModel($request->all());
	$housing_estate_gallery->housing_estates_id = $housing_estate_id; 
	$image_name = $this->getImageName($housing_estate_id, 'gallery', $request->file('gallery_img')->getClientOriginalName());

	$housing_estate_gallery->src =  $request->file('gallery_img')->storeAs('images/housing_estates/gallery', $image_name );
	if($housing_estate_gallery->save()){
	    return $housing_estate_gallery;
	}else{
	    return 0;
	}
    }
    
	/*
	 * метод добавляет фотографию хода строительства дома
	 * принимает изображение, параметры, id дома
	 */
    public function add_progress_image(Request $request, $house_id)
    {
	$house_progress = new HouseProgressModel($request->all());
	$image_name = $this->getImageName($house_id, 'house_progress', $request->file('progress')->getClientOriginalName());
	$house_progress->src =  $request->file('progress')->storeAs('images/house/progress', $image_name );
	$house_progress->save();
	return redirect('/manager/house/'.$house_id.'/house_progress');
    }
    
	/*
	 * Метод создает название загружаемого изображения, принимает
	 * номер категории - type, название объекта, название изображения
	 */
    public function getImageName($id_object, $alias, $image)
    {
	$image_name = $id_object.'_'.$alias.'_'.md5(time());
	$point = strrpos($image, '.');
	$mime = substr($image, $point);
	$image_name = str_replace(' ', '_', $image_name);
	$image_name .= $mime;
	return $image_name;	
    }
    
	/*
	 * меняет порядок отображения планировок дома
	 */
    public function change_plain_order(Request $request, $house_id)
    {
	$house_plains = HousePlainModel::where('house_id', '=', $house_id)->get();
	foreach($request->images_arr as $image){
	    foreach($house_plains as $house_plain){
		if($image['id'] == $house_plain->id){
		    $house_plain->order = $image['order'];
		    $house_plain->update();
		}
	    }
	}
	echo '<h3> Обновлено успешно </h3>';
    }
	/*
	 * Метод изменяет прядок отображения фотографий в галерее жилого комплекса
	 */
    public function change_gallery_images_order(Request $request, $housing_estate_id)
    {
	$gallery_images = HousingEstatesGalleryModel::where('housing_estates_id', '=', $housing_estate_id)->get();
	foreach($request->gallery_images as $image){
	    foreach($gallery_images as $gallery_image ){
		if($image['id'] == $gallery_image->id){
		    $gallery_image->order = $image['order'];
		    $gallery_image->update();
		}
	    }
	}
	echo '<h3> Обновлено успешно </h3>';
    }
    
    
	/*
	 * Метод изменяет прядок отображения фотографий хода строительства
	 */
    public function change_progress_order(Request $request, $house_id)
    {
	$progress_images = HouseProgressModel::where('house_id', '=', $house_id)->get();
	foreach($request->progress_arr as $progress){
	    foreach($progress_images as $progress_image ){
		if($progress['id'] == $progress_image->id){
		    $progress_image->order = $progress['order'];
		    $progress_image->update();
		}
	    }
	}
	echo '<h3> Обновлено успешно </h3>';
    }
    
	/*
	 * метод удаляет картинку из галереи жилого комплекса
	 */
    public function del_housing_estate_gallery_image($housing_estate_id, $image_id)
    {
	$gallery_image = HousingEstatesGalleryModel::find($image_id);

	Storage::delete($gallery_image->src);
	if($gallery_image->delete()){
	    return 1;
	}else{
	    return 0;
	}
    }
	/*
	 * Метод удаляет картинку из планировок дома
	 */
    public function delete_plain_image($house_id,$image_id)
    {
	$house_plain = HousePlainModel::find($image_id);
	Storage::delete($house_plain->src);
	if($house_plain->delete()){
	    return 1;
	}else{
	    return 0;
	}
    }
    
    public function delete_progress_images($house_id, $image_id)
    {
	$progress_image = HouseProgressModel::find($image_id);
	if($progress_image->delete()){
	    return 1;
	}else{
	    return 0;
	}
    }
	
    
	/*
	 * Метод сохраняет картинку - планировку дома, 
	 */
    public function save_plain(Request $request, $house_id)
    {
	$house_plain = new HousePlainModel($request->all());
	$image_name = $this->getImageName($house_id, 'house_plain', $request->file('house_plain')->getClientOriginalName());
	
	$house_plain->src =  $request->file('house_plain')->storeAs('images/house/plains', $image_name );
	$house_plain->save();
	
	return redirect('/manager/house/'.$house_id.'/edit');
    }
    
	/*
	 * метод выводит страницу редактирования хода строительства дома
	 */
    public function show_house_progress($house_id)
    {
	$house_progress = HouseProgressModel::where('house_id', '=', $house_id)
		->select('*', 'house_progress.id AS img_id')
		->leftJoin('month', 'house_progress.month', '=', 'month.id')
		->orderBy('year' )->orderBy('month')->orderBy('order')->get();
	$months = MonthModel::all();

	return view('admin.panel.houses.progress', compact('house_id','house_progress', 'months'));
    }
    
	/*
	 * Метод изменяет название и описание картинки галереи
	 * принимает id ЖК и id картинки
	 */
    public function update_gallery_image_description(Request $request, $housing_estate_id, $image_id)
    {
	$gallery_image = HousingEstatesGalleryModel::find($image_id);
	$gallery_image->description = $request->description;
	$gallery_image->meta_description = $request->meta_description;
	if($gallery_image->update()){
	    echo '<p>Обновлено успешно</p>';
	}else{
	    echo '<p>Обновить не удалось</p>';
	}
    }
    
    
	/*
	 * Метод изменяет название и описание картинки планировки дома
	 * принимает id дома и id картинки
	 */
    public function update_plain_description(Request $request, $house_id, $image_id)
    {
	$house_plain = HousePlainModel::find($image_id);
	$house_plain->description = $request->description;
	$house_plain->meta_description = $request->meta_description;
	if($house_plain->update()){
	    echo '<p>Обновлено успешно</p>';
	}else{
	    echo '<p>Обновить не удалось</p>';
	}
    }
    
    /*
	 * Метод изменяет название и описание картинки планировки дома
	 * принимает id дома и id картинки
	 */
    public function update_progress_description(Request $request, $house_id, $image_id)
    {
	$house_progress = HouseProgressModel::find($image_id);
	$house_progress->description = $request->description;
	$house_progress->meta_description = $request->meta_description;
	if($house_progress->update()){
	    echo '<p>Обновлено успешно</p>';
	}else{
	    echo '<p>Обновить не удалось</p>';
	}
    }
}
