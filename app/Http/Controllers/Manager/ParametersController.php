<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\Parameters\Location_area;
use App\Models\Parameters\ParkingType;
use App\Models\Parameters\MainStructure;
use App\Models\Parameters\WallMaterial;
use App\Models\Parameters\WindowInstallation;
use App\Models\Parameters\CookerType;
use App\Models\Parameters\ElevatorType;
use App\Models\Parameters\ApartmentFinishing;
use App\Models\Parameters\HeatingType;
use App\Models\Parameters\ConstructDate;
use App\Models\Parameters\ApartmentCategoriesModel;
use App\Models\Parameters\ApartmentTypesModel;

class ParametersController extends Controller
{
    public function __construct(){
            // на все методы этого класса будет срабатывать проверка авторизации
        $this->middleware('auth');
    }
    
    public function add_area(Request $request){
        $area =  new Location_area;
        $area->name = $request->name;
        $area->location_region_id = $request->location_region;
        
        $area->save();
    }
    public function add_apartment_finishing(Request $request) {
        $apartment_finishing = new ApartmentFinishing();
        $apartment_finishing->name = $request->name;
//        dump($apartment_finishing);
        if( $apartment_finishing->save() ){
            return 'Тип отделки квартиры сохранен';
        }
    }
    public function add_elevator_type(Request $request) {
        $elevator_type = new ElevatorType;
        $elevator_type->name = $request->name;
 //       dump($elevator_type);
        if( $elevator_type->save() ){
            return 'Тип лифта сохранен';
        }
    }
    public function add_heating_type(Request $request) {
        $heating_type = new HeatingType;
        $heating_type->name = $request->name;
//        dump($heating_type);
        if( $heating_type->save() ){
            return 'Тип отопления сохранен';
        }
    }
    public function add_construct_date(Request $request) {
        $construct_date = new ConstructDate;
        $construct_date->name = $request->name;
        //dump($construct_date);
        if( $construct_date->save() ){
            return 'Дата сохранена';
        }
    }
    
    public function add_cooker_type(Request $request) {
        $cooker_type = new CookerType;
        $cooker_type->name = $request->name;
        //dump($cooker_type);
        if( $cooker_type->save() ){
            return 'Тип кухонной плиты сохранен';
        }
    }
    
    public function add_main_structure(Request $request){
        $main_structure = new MainStructure;
        $main_structure->name = $request->name;
        
        $main_structure->save();
    }
	/*
	 * добавляет категорию квартир
	 */
    public function add_apartment_category(Request $request)
    {
	$apartment_category = new ApartmentCategoriesModel($request->all());
	if($apartment_category->save()){
	    return 1;
	}else{
	    return 0;
	}
    }
    
    public function add_apartment_type(Request $request)
    {
	$apartment_type = new ApartmentTypesModel($request->all());
	if($apartment_type->save()){
	    return 1;
	}else{
	    return 0;
	}
    }
    
    public function add_parking_type(Request $request) {
        $parking_type = new ParkingType;
        $parking_type->name = $request->name;
        
        $parking_type->save();
    }
    public function add_wall_material(Request $request){
        $wall_material =  new WallMaterial;
        $wall_material->name = $request->name;
        //dump($wall_material);      
        if($wall_material->save()){
            return 'Сохранено успешно';
        }
    }
    public function add_window_installation(Request $request){
        $window_installation =  new WindowInstallation;
        $window_installation->name = $request->name;
//        dump($window_installation);      
        if($window_installation->save()){
            return 'Материал окна сохранен';
        }
    }
    
    public function show_area($region_id = null){
        $region_areas = Location_area::where('location_region_id', $region_id)->get()->toArray();

        echo    '<option value="0">Выберите район</option>';
                foreach($region_areas as $region_area){
                    echo '<option value="'.$region_area['id'].'">'.$region_area['name'].'</option>';
                }
       
        //dump($region_area);
    }
    
    public function parameters_page() 
    {
        $location_areas = \App\Models\Parameters\Location_area::all()->toArray();
        $parking_types = \App\Models\Parameters\ParkingType::all()->toArray();
        $main_structures = \App\Models\Parameters\MainStructure::all()->toArray();
        $wall_materials = \App\Models\Parameters\WallMaterial::all()->toArray();
        $window_installations = \App\Models\Parameters\WindowInstallation::all()->toArray();
        $cooker_types = \App\Models\Parameters\CookerType::all()->toArray();
        $elevator_types = \App\Models\Parameters\ElevatorType::all()->toArray();
        $apartment_finishings = \App\Models\Parameters\ApartmentFinishing::all()->toArray();
        $heating_types = \App\Models\Parameters\HeatingType::all()->toArray();
        $construct_dates = \App\Models\Parameters\ConstructDate::all()->toArray();
	$apartment_categories = ApartmentCategoriesModel::all();
	$apartment_types = ApartmentTypesModel::all();
            
        return view('admin.panel.parameters_page', compact('location_areas', 'parking_types', 'main_structures', 'wall_materials', 'window_installations', 'cooker_types', 'elevator_types', 'apartment_finishings', 'heating_types', 'construct_dates', 'apartment_categories', 'apartment_types'));
    }
    
    public function update_parameter(Request $request, $parameter_id, $table_name)
    {
	if( DB::table($table_name)->where('id', $parameter_id)->update(['name'=>$request->name]) ){
	    return 1;
	}else{
	    return 0;
	}
	
    }

}
