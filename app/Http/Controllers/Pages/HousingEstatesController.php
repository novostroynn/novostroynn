<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\Parameters\LocationRegions;
use App\Models\Parameters\Location_area;
use App\Models\Parameters\ApartmentCategoriesModel;
use App\Models\Parameters\ConstructDate;
use App\Models\Parameters\ApartmentFinishing;
use App\Models\Manager\HousingEstatesModel;

class HousingEstatesController extends Controller
{
    public function index() {
	$locations = LocationRegions::all();
	$location_areas = Location_area::where('location_region_id', '=', 1)->get();
	$apartment_categories = ApartmentCategoriesModel::all();
	$construct_dates = ConstructDate::all();
	$apartment_finishings = ApartmentFinishing::all();
//	$housing_estates = HousingEstatesModel::all();
	$housing_estates = DB::table('housing_estate')
		->select('housing_estate.*', 'location_areas.name AS location_name', 'construct_dates.name AS end_construct')
		->leftJoin('location_areas', 'housing_estate.location_area', '=', 'location_areas.id')
		->leftJoin('construct_dates', 'housing_estate.end_construct', '=', 'construct_dates.id')
		->get();
	
	$coords = array();
	foreach ($housing_estates as $k => $housing_estate){
	    $coords[] = ['latitude'=>$housing_estate->latitude, 'longitude'=>$housing_estate->longitude, 'name'=>$housing_estate->name, 'logo_src'=>$housing_estate->logo_src, 'address'=>$housing_estate->address, 'end_construct'=>$housing_estate->end_construct, 'location_name'=>$housing_estate->location_name];
	}
	$coordinates = json_encode($coords);
	
	return view('pages.housing_estates.index', compact('locations', 'location_areas', 'apartment_categories', 'construct_dates', 'apartment_finishings', 'coordinates'));
    }
}
