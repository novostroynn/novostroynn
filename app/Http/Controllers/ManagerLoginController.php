<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
/*
 * класс отвечает за оавторизацию администраторов
 */
class ManagerLoginController extends Controller
{
    
    use AuthenticatesUsers;
    
    
    
//    public function authenticate(Request $request) {
//       
//        if(Auth::attempt(['login'=> $request->login, 'password'=> $request->password ])){
//            return redirect()->intended('dashboard');
//        }
//    }
    
//    public function __construct()
//    {
//        $this->middleware('guest')->except('logout');
//    }
    
    public function logout() {
        Auth::logout();
        return redirect('/manager');   
    }
}
